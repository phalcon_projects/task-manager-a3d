'use strict'
module.exports = {
  NODE_ENV: '"production"',
  SERVICE_BASE_URL: '"http://service-task-manager-a3d.happyuser.info:9000"',
  SERVICE_REST_API_URL: '"http://service-task-manager-a3d.happyuser.info:9000/api/v1"',
  SERVICE_MESSAGING: '"service-task-manager-a3d.happyuser.info:9001"'
}
