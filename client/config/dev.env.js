'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  SERVICE_BASE_URL: '"http://task-service.visionlab.loc:80"',
  SERVICE_REST_API_URL: '"http://task-service.visionlab.loc:80/api/v1"',
  SERVICE_MESSAGING: '"messaging-service.loc:8000"'
})
