'use strict'
module.exports = {
  NODE_ENV: '"production"',
  SERVICE_BASE_URL: '"http://task-service.visionlab.de"',
  SERVICE_REST_API_URL: '"http://task-service.visionlab.de/api/v1"',
  SERVICE_MESSAGING: '"task-service.visionlab.de:9001"'
}
