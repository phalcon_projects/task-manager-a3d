class Storage {
    constructor() {
        this._properties = {};
    }

    /**
     * Get the value from storage.
     *
     * @param {String} key The key for which the storage is searched.
     * @param {*} defaultValue The default value returned if the search result is null.
     * @returns {*}
     */
    get(key, defaultValue = null) {
        if (window.localStorage[key] && window.localStorage[key] !== 'undefined') {
            let type = String;

            for (const keyItem in this._properties) {
                if (keyItem === key) {
                    type = this._properties[key].type;
                    break;
                }
            }

            return this._process(type, window.localStorage[key]);
        }

        return defaultValue !== null ? defaultValue : null;
    }

    /**
     * Set storage value by key.
     *
     * @param {String} key The key for which the data storing in the storage is associated.
     * @param {*} value The storing data value.
     */
    set(key, value) {
        window.localStorage.setItem(key, JSON.stringify(value));
        // TODO: DEBUG CHECK VALUE SET
        return;

        for (const keyItem in this._properties) {
            const type = this._properties[key].type;

            if (keyItem === key && [Array, Object].includes(type)) {
            }
        }

        window.localStorage.setItem(key, value);
    }

    /**
     * Add new property to the storage.
     *
     * @param {String} key The key for which the new property is storing to the storage.
     * @param {function} type The property type.
     * @param {*} defaultValue The default value assigned to the property.
     */
    add(key, type, defaultValue = undefined) {
        type = type || String;

        this._properties[key] = {type};

        if (!window.localStorage[key] && defaultValue !== null) {
            window.localStorage.setItem(
                key,
                [Array, Object].includes(type) ? JSON.stringify(defaultValue) : defaultValue
            );
        }
    }

    /**
     * Remove value from storage.
     *
     * @param {String} key The key for which the storage data should be removed.
     */
    remove(key) {
        return window.localStorage.removeItem(key);
    }

    /**
     * Clear the storage.
     */
    clear() {
        window.localStorage.clear();
    }

    /**
     * Process the value before return it from storage.
     *
     * @param {String} type The data type.
     * @param {*} value The data value.
     * @returns {*}
     * @private
     */
    _process(type, value) {
        switch (type) {
            case Boolean:
                return value === 'true';
            case Number:
                return parseInt(value);
            case Array:
                try {
                    const array = JSON.parse(value);
                    return Array.isArray(array) ? array : [];
                } catch (e) {
                    return [];
                }
            case Object:
                try {
                    return JSON.parse(value);
                } catch (e) {
                    return {};
                }
            case String:
                try {
                    return JSON.parse(value);
                } catch (e) {
                    return {};
                }
            default:
                return value;
        }
    }
}

export default Storage;
