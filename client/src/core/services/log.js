class LogService {

  /**
   * Create a new LogService instance.
   *
   * @param parameters
   */
  constructor(parameters) {
    this.isDebugMode = parameters.enabled || false
  }

  log(data, mode = 'console') {
    if (mode === 'console' && this.isDebugMode) {
      console.log(data)
    }
  }

  error(data, mode = 'console') {
    if (mode === 'console' && this.isDebugMode) {
      console.error(data)
    }
  }
}

export default LogService
