class Event {

    /**
     * Create a new event instance.
     */
    constructor() {
        this.instance = new Vue();
    }

    /**
     * Register event emit and its data to fire further.
     *
     * @param event The event name to register firing.
     * @param data The data assigning to the further callback calling after event is happened.
     */
    emit(event, data = null) {
        this.instance.$emit(event, data);
    }

    /**
     * Register event firing with the callback.
     *
     * @param event The event name to register firing.
     * @param callback A method that calling when the event is firing.
     */
    on(event, callback) {
        this.instance.$on(event, callback);
    }
}

export default Event;