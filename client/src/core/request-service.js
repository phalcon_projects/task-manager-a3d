class RequestService {
  /**
   * @typedef {Object} CallParameters
   * @property {String} path
   * @property {String} [method=get]
   * @property {Object} [headers]
   * @property {*} [data]
   * @property {Object} [parameters]
   */

  /**
   * Process GET request for fetching the entity list.
   *
   * @param {CallParameters} parameters
   * @return {Promise<any>}
   */
  static get (parameters) {
    parameters.method = 'get'
    return this.request(parameters)
  }

  /**
   * Process the POST request.
   *
   * @param {CallParameters} parameters
   * @return {Promise<any>}
   */
  static post (parameters) {
    parameters.method = 'post'
    return this.request(parameters)
  }

  /**
   * Process the PUT request.
   *
   * @param {CallParameters} parameters
   * @return {Promise<any>}
   */
  static put (parameters) {
    parameters.method = 'put'
    return this.request(parameters)
  }

  /**
   * Process the PATCH request.
   *
   * @param {CallParameters} parameters
   * @return {Promise<any>}
   */
  static patch (parameters) {
    parameters.method = 'patch'
    return this.request(parameters)
  }

  /**
   * Process the DELETE request.
   *
   * @param {CallParameters} parameters
   * @return {Promise<any>}
   */
  static delete (parameters) {
    parameters.method = 'delete'
    return this.request(parameters)
  }

  /**
   * The main request method for processing REST API service interaction.
   *
   * @param {CallParameters} options
   * @return {Promise<any>}
   */
  static request (options) {
    let {path, method, data, parameters, headers, baseURL} = options
    let self = this
    let baseURLPath = baseURL ? baseURL : window.Config.serviceRestApiUrl

    return new Promise((resolve, reject) => {
      let options = {
        url: `${baseURLPath}/${path}`,
        method: method,
        data: data,
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        json: true,
        onUploadProgress: (progressEvent) => {
          if (progressEvent.lengthComputable) {
            let progress = Math.round(progressEvent.loaded * 100 / progressEvent.total)
            window.Event.emit('progress', progress)
            console.log('progress', progress)
          } else {
            // Unable to compute progress information since the total size is unknown
            window.Event.emit('progress', false)
          }
        }
      }

      let token = window.Auth.getToken()

      if (token) {
        options.headers['Authorization'] = `Bearer ${token}`
      }

      if (headers) {
        options.headers = headers
      }

      // TODO: process parameters further
      if (parameters) {
      }

      window.axios(options)
        .then(response => {
          self.onSuccess(response.data)

          resolve(response.data)
        })
        .catch(error => {
          console.error('error', error)

          self.onFail(error.response)

          reject(error.response)
        })
    })
  }

  /**
   * Handling successful response.
   *
   * @param data
   */
  static onSuccess (data) {
    console.log('DEBUG: REQUEST SERVICE: success ', data)// TODO: DEBUG
  }

  /**
   * Handling failed response.
   *
   * @param errors
   * @return {*}
   */
  static onFail (errors) {
    if (errors && errors.error && errors.error.code === 2030) {
      window.Auth.clear()
      console.error(errors.error.message)
    }

    return errors
  }

}

export default RequestService
