import Errors from './errors';

class Form {

    /**
     * Create a new Form instance.
     *
     * @param {object} data
     */
    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();
    }

    /**
     * Fetch all relevant data for the form.
     */
    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }

    /**
     * Reset the form fields.
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }

        this.errors.clear();
    }

    /**
     * Submit the form.
     *
     * @param parameters
     * @return {Promise<any>}
     */
    submit(parameters) {
        let {requestPath, requestMethod, requestHeaders} = parameters;

        return new Promise((resolve, reject) => {
            window.RequestService.request({
                path: requestPath,
                method: requestMethod,
                data: this.data(),
                headers: requestHeaders
            })
                .then(response => {
                    this.onSuccess(response);

                    resolve(response);
                })
                .catch(response => {
                    let errors;

                    if (response) {
                        if (response.response && response.response.data) {
                            errors = response.response.data;
                        } else if (response.errors) {
                            errors = response.errors;
                        } else if (response.message) {
                            errors = response.message;
                        } else {
                            errors = 'Error is happened.';
                        }
                    } else {
                        errors = 'Error is happened.';
                    }

                    this.setErrros(errors);

                    reject(errors);
                });
        });
    }

    /**
     * Handle a successful form submission.
     *
     * @param {object} data
     */
    onSuccess(data) {
        console.log(data);
        this.reset();
    }

    /**
     * Handle a failed form submission.
     *
     * @param {object} errors
     */
    setErrros(errors) {
        this.errors.set(errors);
    }
}

export default Form;
