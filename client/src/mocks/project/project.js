import MockRequestService from '../mock-request-service';

class MockProjectService extends MockRequestService {

    /**
     * Set response result status type is success or not.
     *
     * @return {boolean}
     */
    static isMockResponseTypeSuccess() {
        return true;
    }

    /**
     * The entity for which the request is processing.
     */
    static getEntity() {
        return 'project';
    }
}

export default MockProjectService;