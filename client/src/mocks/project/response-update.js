module.exports = {
    id: 1,
    title: 'Example project title',
    description: 'Example project description',
    last_update: '10.01.2018 - 12:10',
    status: 'completed',
    termin: '10.02.2018',
    tasks: [
        {
            id: 10,
            title: 'Task title',
            description: 'Task description'
        }
    ]
};