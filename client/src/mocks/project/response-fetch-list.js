module.exports = [
    {
        id: 1,
        title: 'Example project title',
        description: 'Example project description',
        last_update: '10.01.2018 - 12:10',
        status: 'completed',
        term: '10.02.2018',
        tasks: [
            {
                id: 10,
                title: 'Task title',
                description: 'Task description'
            }
        ]
    },
    {
        id: 2,
        title: 'Example project 2',
        last_update: '10.01.2018 - 12:10',
        status: 'active',
        term: '21.01.2018',
        tasks: [
            {
                id: 12,
                title: 'Task 12 title',
                description: 'Task 12 description'
            }
        ]
    },
    {
      id: 3,
      title: 'Example project title 3',
      last_update: '10.01.2018 - 12:10',
      status: 'active',
      termin: '21.01.2019',
      tasks: [
        {
          id: 12,
          title: 'Task 10 title',
          description: 'Task 10 description'
        }
      ]
    }
];
