const ERROR_ABSTRACT_METHOD_CALL = 'The abstract method should be declared in the ancestor class.';

class MockRequestService {

    /**
     * Process GET request for fetching the entity list.
     *
     * @return {Promise<any>}
     */
    static fetchList() {
        return this.request('fetch-list');
    }

    /**
     * Process the GET request with id parameter.
     *
     * @param id The entity item id for which request is processing.
     * @return {Promise<any>}
     */
    static fetch(id) {
        return this.request('fetch');
    }

    /**
     * Process the POST request.
     *
     * @param data The entity item data to which is used to process item creation.
     * @return {Promise<any>}
     */
    static create(data) {
        return this.request('create');
    }

    /**
     * Process the PUT request.
     *
     * @param data The entity item data to which is used to process item updating.
     * @return {Promise<any>}
     */
    static update(data) {
        return this.request('update');
    }

    /**
     * Process the DELETE request.
     *
     * @param id The entity item id for which deletion is processing.
     * @return {Promise<any>}
     */
    static delete(id) {
        return this.request('delete');
    }

    /**
     * Return response result status type is success or not.
     *
     * @return {boolean}
     */
    static isMockResponseTypeSuccess() {
        throw new Error(ERROR_ABSTRACT_METHOD_CALL + ' Method: isMockResponseTypeSuccess');
    }

    /**
     * The entity for which the request is processing.
     */
    static getEntity() {
        throw new Error(ERROR_ABSTRACT_METHOD_CALL + ' Method: getEntity');
    }

    /**
     * The response data returned as response for the request.
     * The corresponded files should be present at the folder with processing entity name.
     * For example the structure should looks like:
     * - directory "project" with content:
     * -- response-create.js
     * -- response-delete.js
     * -- response-fetch.js
     * -- response-fetch-list.js
     * -- response-update.js
     *
     * @param method The processed request method.
     * @return {*}
     */
    static getMockResponseData(method) {
        const entity = this.getEntity();

        return require(`./${entity}/response-${method}`);
    }

    /**
     * The main request method for processing response mock entity data.
     *
     * @param method The processed request method.
     * @return {Promise<any>}
     */
    static request(method) {
        return new Promise((resolve, reject) => {
            if (this.isMockResponseTypeSuccess()) {
                const responseData = this.getMockResponseData(method);

                this.onSuccess(responseData);
                resolve(responseData);
            } else {
                this.onFail({error: 'Error is happened'});
                reject({error: 'Error is happened'});
            }
        });
    }

    static onSuccess(data) {
        console.log('DEBUG: MOCK REQUEST SERVICE: success ', data);// TODO: DEBUG
    }

    static onFail(errors) {
        return errors;
    }

}

export default MockRequestService;