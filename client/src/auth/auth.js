class Auth {
  constructor() {
  }

  /**
   * @typedef {Object} UserData
   * @property {Number} id
   * @property {String} login
   * @property {String} email
   * @property {String} name
   * @property {{id:string,alias:string,title:string}} state
   * @property {String} created_at
   * @property {String} updated_at
   * @property {{id:string,alias:string,title:string,color:string}} role
   * @property {Object} [projects]
   */
  /**
   *
   * @return {Promise<UserData>}
   */
  static get() {
    return new Promise((resolve, reject) => {
      let instance = this.getInstance()

      if (instance) {
        resolve(instance)
      } else {
        let token = this.getToken()

        if (token) {
          window.RequestService.get({
            path: '/users/profile', parameters: {
              headers: {
                'Authorization': `Bearer ${token}`
              }
            }
          }).then((response) => {
            if (response && response.data && response.data.user) {
              window.LocalStorage.set('auth', {'instance': response.data.user})
              resolve(response.data.user)
            }
          }).catch((error) => {
            console.error(error)
            reject(error)
          })
        } else {
          resolve(null)
        }
      }
    })
  }

  static set(data) {
    window.LocalStorage.set('auth', data)
  }

  static setInstance(data) {
    let auth = this.getAuth()
    auth.instance = data
    window.LocalStorage.set('auth', auth)
  }

  static getInstance() {
    let auth = this.getAuth()
    return (auth && auth.instance) ? auth.instance : null
  }

  static getToken() {
    let auth = this.getAuth()
    return (auth && auth.token) ? auth.token : null
  }

  static sessionExpired() {
    let auth = this.getAuth()

    if (auth) {
      let expireDate = moment.unix(auth.expire / 1000).format('YYYY-MM-DD')
      return moment(expireDate).diff(moment().format('YYYY-MM-DD'), 'days') < 0
    }

    return true
  }

  static getAuth() {
    return window.LocalStorage.get('auth')
  }

  static clear() {
    return new Promise((resolve, reject) => {
      window.LocalStorage.remove('auth')
      resolve()
    })
  }
}

export default Auth
