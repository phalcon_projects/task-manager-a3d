let Config = {
  app: {
    name: 'Messaging',
    localStorePrefix: 'messaging'
  },

  // TODO: LogService mode. Remove in production.
  debug: true,

  // The REST API service base url that is used to communicate with server side
  serviceBaseUrl: process.env.SERVICE_BASE_URL,
  serviceRestApiUrl: process.env.SERVICE_REST_API_URL,

  // Socket messaging service parameters
  socketServer: {
    url: 'http://messaging.loc:8000',
    pathPrefix: 'api/v1'
  }
}

export default Config
