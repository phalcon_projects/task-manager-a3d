// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './bootstrap'

import App    from './App'
import router from './router'
import store  from './store/index'

Vue.config.productionTip = false
Vue.component('alert', require('./components/alert/alert'))

export const app = new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<app/>',
})
