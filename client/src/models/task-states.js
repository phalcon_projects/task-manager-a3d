export const STATE_DRAFT = 0;
export const STATE_ACTIVE = 1;
export const STATE_COMPLETED = 2;
export const STATE_DELETED = 3;
export const STATE_OUTDATED = 4;

let states = [];
states[STATE_DRAFT] = {
    id: STATE_DRAFT,
    alias: 'draft',
    title: 'Draft',
    color: '#f96332'
};
states[STATE_ACTIVE] = {
    id: STATE_ACTIVE,
    alias: 'active',
    title: 'Active',
    color: '#2CA8FF'
};
states[STATE_COMPLETED] = {
    id: STATE_COMPLETED,
    alias: 'completed',
    title: 'Completed',
    color: '#18ce0f'
};
states[STATE_DELETED] = {
    id: STATE_DELETED,
    alias: 'deleted',
    title: 'Deleted',
    color: '#B8B8B8'
};
states[STATE_OUTDATED] = {
    id: STATE_OUTDATED,
    alias: 'outdated',
    title: 'Outdated',
    color: '#fe0000'
};

export const STATES = states;
