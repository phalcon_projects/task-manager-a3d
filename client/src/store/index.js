import Vue  from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showing: true,
    collapsed: false,
    fullHeight: false
  },

  getters: {
    isShowed: state => {
      return state.showing
    }
  },

  mutations: {
    show(state) {
      state.showing = !state.showing
    },

    toggleCollapsed(state) {
      state.collapsed = !state.collapsed
    },

    toggleFullHeight(state) {
      state.fullHeight = !state.fullHeight
    }
  },

  actions: {}
})
