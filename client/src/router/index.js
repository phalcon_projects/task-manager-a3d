import VueRouter from 'vue-router'
import ROUTES    from './routes'

let router = new VueRouter({
  routes: ROUTES
})

router.beforeResolve((to, from, next) => {
  if (!to.matched.length) {
    next('/not-found')
  } else {
    let auth = window.Auth.getInstance()

    if (auth && window.Auth.sessionExpired()) {
      window.Auth.clear()
        .then(() => {
          window.Event.emit('sessionClosed')
          next('/sign-in')
        })
    }

    if (to.matched.some(route => route.meta.requireAuth)) {
      if (!auth) {
        let token = window.Auth.getToken()

        if (!token) {
          next({path: '/sign-in', query: {return: to.path}})
        } else {
          window.Auth.get()
            .then((response) => {
              next()
            }).catch((error) => {
            console.error(error)
            next('/sign-in')
          })
        }
      } else {
        next()
      }
    } else if (to.matched.some(route => route.meta.requireNotAuth)) {
      auth ? next('/') : next()
    }

    next()
  }
})

export default router
