import PageNotifications   from '../views/NotificationList'
import PageNotFound        from '../views/NotFound'
import PageOverview        from '../views/Overview'
import PageProfile         from '../views/Profile'
import PageProjectDetail   from '../views/ProjectDetail'
import PageProjectSettings from '../components/project/settings'
import PageProjectReviews  from '../components/project/reviews'
import PageSignIn          from '../views/SignIn'
import PageSubTask         from '../components/task/sub-task/detail'
import PageSystemSettings  from '../views/admin/settings'
import PageTaskDetail      from '../components/task/detail'
import PageTimeline        from '../views/Timeline'

let routes = [
  {
    path: '/sign-in',
    name: 'SignIn',
    component: PageSignIn,
    meta: {
      requireNotAuth: true
    }
  },
  {
    path: '/',
    redirect: 'overview',
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/overview',
    name: 'Overview',
    component: PageOverview,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/profile',
    name: 'PageProfile',
    component: PageProfile,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/not-found',
    name: 'PageNotFound',
    component: PageNotFound
  },
  {
    path: '/project/:id',
    name: 'Project',
    component: PageProjectDetail,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/project/:project_id/task/:task_id',
    name: 'Task',
    component: PageTaskDetail,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/project/:project_id/task/:task_id/timeline',
    name: 'PageTimeline',
    component: PageTimeline,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/project/:project_id/task/:task_id/sub-task/:sub_task_id',
    name: 'SubTask',
    component: PageSubTask,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/project/:project_id/settings',
    name: 'PageProjectSettings',
    component: PageProjectSettings,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/project/:project_id/reviews',
    name: 'PageProjectReviews',
    component: PageProjectReviews,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/notification-list',
    name: 'PageNotifications',
    component: PageNotifications,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/admin/settings',
    name: 'PageSystemSettings',
    component: PageSystemSettings,
    meta: {
      requireAuth: true,
      requireAdmin: true
    }
  }
]

export default routes
