window._ = require('lodash')

try {
  window.$ = window.jQuery = require('jquery')
  window.Popper = require('popper.js').default
  require('bootstrap')
} catch (e) {
  console.error(e)
}

import Vue            from 'vue'
import VueRouter      from 'vue-router'
import axios          from 'axios'
import moment         from 'moment'
import Config         from './config/config'
import Event          from './core/event'
import LocalStorage   from './core/storage'
import RequestService from './core/request-service'
import EntityService  from './services/entity'
import Auth           from './auth/auth'
import Form           from './core/form'
import VCalendar      from 'v-calendar'
import 'v-calendar/lib/v-calendar.min.css'

// Use v-calendar, v-date-picker & v-popover components
Vue.use(VCalendar)

window.Vue = Vue
Vue.use(VueRouter)

window.axios = axios
window.axios.defaults.headers.common = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}
window.moment = moment

window.Config = Config
window.Event = new Event()
window.LocalStorage = new LocalStorage()
window.RequestService = RequestService
window.EntityService = EntityService
window.Auth = Auth

window.Form = Form
