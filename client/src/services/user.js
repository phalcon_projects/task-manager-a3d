import EntityService from '../services/entity';

const SERVICE_PATH = 'users';

class UserService extends EntityService {

    static authenticate() {
        super.post({
            path: `${SERVICE_PATH_PREFIX}/authenticate`,
            headers: {}
        });
    }

}
