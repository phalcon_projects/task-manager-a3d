import RequestService from '../core/request-service'

const ERROR_ABSTRACT_METHOD_CALL = 'The abstract method should be declared in the ancestor class.'
const ERROR_MISSED_REQUIRED_ARGUMENT = 'The argument should be specified for the class constructor.'

class EntityService extends RequestService {

  constructor (path) {
    super()

    if (!path) {
      throw new Error(ERROR_MISSED_REQUIRED_ARGUMENT + ' Argument: path')
    }

    this.servicePath = path
  }

  /**
   * @return {String}
   */
  getServicePathPrefix () {
    return this.servicePath
    //throw new Error(ERROR_ABSTRACT_METHOD_CALL + ' Method: getServicePathPrefix');
  }

  /**
   * Process GET request for fetching the entity list.
   *
   * @param parameters
   * @return {Promise<any>}
   */
  fetchList (parameters = {}) {
    parameters.path = this.getServicePathPrefix()

    if (parameters.query) {
      parameters.path += `?${parameters.query}`
    }

    return RequestService.get(parameters)
  }

  /**
   * Process the GET request with id parameter.
   *
   * @param parameters
   * @return {*}
   */
  fetch (parameters) {
    parameters.path = `${this.getServicePathPrefix()}/${parameters.id}`

    if (parameters.query) {
      parameters.path += `?${parameters.query}`
    }

    return RequestService.get(parameters)
  }

  /**
   * Process the POST request.
   *
   * @param item
   * @return {*}
   */
  create (item) {
    return RequestService.post({path: this.getServicePathPrefix(), data: item})
  }

  /**
   * Process the PUT request.
   *
   * @param item
   * @return {*}
   */
  update (item) {
    return RequestService.put({path: `${this.getServicePathPrefix()}/${item.id}`, data: item})
  }

  /**
   * Process the DELETE request.
   *
   * @param id
   * @return {*}
   */
  delete (id) {
    return RequestService.delete({path: `${this.getServicePathPrefix()}/${id}`})
  }

}

export default EntityService

