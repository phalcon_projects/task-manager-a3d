<?php

namespace APP\REST\Constants;

class Services extends \APP\API\Constants\Services
{
    // APP\REST
    const FRACTAL_MANAGER = 'fractalManager';
    const PHQL_QUERY_PARSER = 'phqlQueryParser';
}

