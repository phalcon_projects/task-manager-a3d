<?php

namespace APP\REST\Di;

use APP\API\Constants\ErrorCodes;
use APP\REST\Constants\Services;
use APP\REST\QueryParsers\PhqlQueryParser;
use APP\API\Exception;

class FactoryDefault extends \APP\API\Di\FactoryDefault
{
    public function __construct()
    {
        parent::__construct();

        $this->setShared(Services::FRACTAL_MANAGER, function () {

            $className = '\League\Fractal\Manager';

            if (!class_exists($className)) {
                throw new Exception(ErrorCodes::GENERAL_SYSTEM, null,
                    '\League\Fractal\Manager was requested, but class could not be found');
            }

            return new $className();
        });

        $this->setShared(Services::PHQL_QUERY_PARSER, new PhqlQueryParser);
    }
}
