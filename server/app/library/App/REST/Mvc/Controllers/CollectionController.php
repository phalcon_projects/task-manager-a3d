<?php

namespace APP\REST\Mvc\Controllers;

class CollectionController extends FractalController
{
    /** @var \APP\REST\Api\ApiCollection */
    protected $_collection;

    /** @var \APP\REST\Api\ApiEndpoint */
    protected $_endpoint;

    /**
     * @return \APP\REST\Api\ApiCollection
     */
    public function getCollection()
    {
        if (!$this->_collection) {
            $this->_collection = $this->application->getMatchedCollection();
        }

        return $this->_collection;
    }

    /**
     * @return \APP\REST\Api\ApiEndpoint
     */
    public function getEndpoint()
    {
        if (!$this->_endpoint) {
            $this->_endpoint = $this->application->getMatchedEndpoint();
        }

        return $this->_endpoint;
    }
}
