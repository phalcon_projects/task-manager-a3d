<?php

namespace APP\REST\Transformers\Documentation;

use Phalcon\Mvc\Router\Route;
use APP\REST\Transformers\Transformer;

class RouteTransformer extends Transformer
{
    public function transform(Route $route)
    {
        return [
            'name' => $route->getName(),
            'pattern' => $route->getPattern()
        ];
    }
}
