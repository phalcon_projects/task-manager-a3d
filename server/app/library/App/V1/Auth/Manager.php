<?php

namespace App\V1\Auth;

use APP\API\Auth\Session;
use APP\API\Exception;

class Manager extends \APP\API\Auth\Manager
{
    const LOGIN_DATA_EMAIL = 'email';

    /**
     * @param string $accountTypeName
     * @param string $email
     * @param string $password
     *
     * @return Session Created session
     * @throws Exception
     *
     * Helper to login with email & password
     */
    public function loginWithEmailPassword($accountTypeName, $email, $password)
    {
        try {
            return $this->login($accountTypeName, [
                self::LOGIN_DATA_EMAIL => $email,
                self::LOGIN_DATA_PASSWORD => $password
            ]);
        } catch (Exception $e) {
            throw $e;
        }
    }
}