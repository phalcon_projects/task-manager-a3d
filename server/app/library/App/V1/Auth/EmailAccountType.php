<?php

namespace App\V1\Auth;

use App\V1\Constants\Services;
use App\V1\Model\User;
use Phalcon\Di;
use Phalcon\Security;

class EmailAccountType implements \APP\API\Auth\AccountType
{
    const NAME = 'email';

    public function login($data)
    {
        /** @var Security $security */
        $security = Di::getDefault()->get(Services::SECURITY);

        $email = $data[Manager::LOGIN_DATA_EMAIL];
        $password = $data[Manager::LOGIN_DATA_PASSWORD];

        /** @var User $user */
        $user = User::findFirst([
            'conditions' => 'email = :email:',
            'bind' => ['email' => $email]
        ]);

        if (!$user) {
            return null;
        }

        if (!$security->checkHash($password, $user->password)) {
            return null;
        }

        return (string)$user->id;
    }

    public function authenticate($identity)
    {
        return User::count([
            'conditions' => 'id = :id:',
            'bind' => ['id' => (int)$identity]
        ]) > 0;
    }
}