<?php

namespace App\V1\Auth;

use App\V1\Constants\Services;
use App\V1\Model\User;
use APP\API\Auth\AccountType;
use Phalcon\Di;
use Phalcon\Security;

class UsernameAccountType implements AccountType
{
    const NAME = 'login';

    public function login($data)
    {
        /** @var Security $security */
        $security = Di::getDefault()->get(Services::SECURITY);

        $username = $data[Manager::LOGIN_DATA_LOGIN];
        $password = $data[Manager::LOGIN_DATA_PASSWORD];

        /** @var User $user */
        $user = User::findFirst([
            'columns' => 'id, login, email, name, password, role, state, created_at, updated_at',
            'conditions' => 'login = :login:',
            'bind' => ['login' => $username]
        ]);

        if (!$user) {
            return null;
        }

        //$security->setDefaultHash(\Phalcon\Security::CRYPT_MD5);

        if (!$security->checkHash($password, $user->password)) {
            return null;
        }

        return (string)$user->id;
    }

    public function authenticate($identity)
    {
        return User::count([
            'conditions' => 'id = :id:',
            'bind' => ['id' => (int)$identity]
        ]) > 0;
    }
}
