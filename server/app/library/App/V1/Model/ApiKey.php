<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class ApiKey extends DateTrackingModel
{
    public $id;
    public $value;
    public $state;

    /* region States */
    const STATE_DISABLED = 0;
    const STATE_ACTIVE = 1;

    const STATES = [
        self::STATE_DISABLED => [
            'id' => self::STATE_DISABLED,
            'alias' => 'disabled',
            'title' => 'Disabled'
        ],
        self::STATE_ACTIVE => [
            'id' => self::STATE_ACTIVE,
            'alias' => 'active',
            'title' => 'Active'
        ]
    ];
    /* endregion States */

    public function getSource()
    {
        return 'api_keys';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'value' => 'value',
                'state' => 'state'
            ];
    }
}
