<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class DiscussionCategory extends DateTrackingModel
{
    public $id;
    public $object_id;
    public $title;
    public $alias;

    public function getSource()
    {
        return 'discussion_categories';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'object_id' => 'object_id',
                'title' => 'title',
                'alias' => 'alias'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('object_id', Discussion::class, 'id', [
            'alias' => 'Discussion',
        ]);
    }

    protected function excludedProperties()
    {
        return ['object_id'];
    }
}
