<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Activity extends DateTrackingModel
{
    public $id;
    public $object_id;
    public $object_type;
    public $related_id;
    public $creator_id;
    public $related_user_id;
    public $project_id;
    public $type;
    public $content;
    public $state;

    const STATE_DELETED = 0;
    const STATE_ACTIVE = 1;
    const STATE_DRAFT = 2;

    const STATES = [
        self::STATE_DELETED => ['id' => self::STATE_DELETED, 'alias' => 'deleted', 'title' => 'Deleted'],
        self::STATE_DRAFT => ['id' => self::STATE_DRAFT, 'alias' => 'draft', 'title' => 'Draft'],
        self::STATE_ACTIVE => ['id' => self::STATE_ACTIVE, 'alias' => 'active', 'title' => 'Active']
    ];

    public function getSource()
    {
        return 'activities';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'object_id' => 'object_id',
                'object_type' => 'object_type',
                'related_id' => 'related_id',
                'creator_id' => 'creator_id',
                'related_user_id' => 'related_user_id',
                'type' => 'type',
                'content' => 'content',
                'project_id' => 'project_id',
                'state' => 'state'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('creator_id', User::class, 'id', ['alias' => 'Creator']);
        $this->belongsTo('related_user_id', User::class, 'id', ['alias' => 'Related User']);
        $this->belongsTo('project_id', Project::class, 'id', ['alias' => 'Project']);
    }

    protected function excludedProperties()
    {
        return ['object_id', 'creator_id', 'project_id'];
    }
}
