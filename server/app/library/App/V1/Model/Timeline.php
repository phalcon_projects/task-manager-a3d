<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Timeline extends DateTrackingModel
{
    public $id;
    public $start_date;
    public $object_id;
    public $object_type;

    public function getSource()
    {
        return 'timelines';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'start_date' => 'start_date',
                'object_id' => 'object_id',
                'object_type' => 'object_type'
            ];
    }

    public function initialize()
    {
    }

    protected function excludedProperties()
    {
        return [];
    }
}
