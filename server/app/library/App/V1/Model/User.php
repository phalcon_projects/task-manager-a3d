<?php

namespace App\V1\Model;

use App\V1\Constants\AclRoles;
use App\V1\Mvc\DateTrackingModel;

class User extends DateTrackingModel
{
    public $id;
    public $login;
    public $email;
    public $password;
    public $name;
    public $role;
    public $state;
    public $access_token;
    public $access_token_expire;
    public $refresh_token;

    const USER_ROLES = [
        AclRoles::ADMINISTRATOR => [
            'id' => AclRoles::ADMINISTRATOR,
            'title' => 'Administrator',
            'alias' => 'administrator',
            'color' => '#ff3636'
        ],
        AclRoles::USER => [
            'id' => AclRoles::USER,
            'title' => 'User',
            'alias' => 'user',
            'color' => '#2ca8ff'
        ]
    ];

    const STATE_PENDING = 0;
    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 2;
    const STATE_DELETED = 3;

    const USER_STATES = [
        self::STATE_PENDING => [
            'id' => self::STATE_PENDING,
            'alias' => 'pending',
            'title' => 'Pending'
        ],
        self::STATE_ACTIVE => [
            'id' => self::STATE_ACTIVE,
            'alias' => 'active',
            'title' => 'Active'
        ],
        self::STATE_INACTIVE => [
            'id' => self::STATE_INACTIVE,
            'alias' => 'inactive',
            'title' => 'Inactive'
        ],
        self::STATE_DELETED => [
            'id' => self::STATE_DELETED,
            'alias' => 'deleted',
            'title' => 'Deleted'
        ]
    ];

    public function getSource()
    {
        return 'users';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'email' => 'email',
                'login' => 'login',
                'name' => 'name',
                'password' => 'password',
                'role' => 'role',
                'state' => 'state'
            ];
    }

    public function initialize()
    {
        $this->hasOne('creator_id', Attachment::class, 'avatar', ['alias' => 'Attachment']);
    }
}
