<?php

namespace App\V1\Model;

class DiscussionFollower extends Follower
{
    public $user_id;
    public $object_id;
    public $object_type = 'discussion';
    public $state;

    public function getSource()
    {
        return 'followers';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'user_id' => 'user_id',
                'object_id' => 'object_id',
                'object_type' => 'object_type',
                'state' => 'state'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('object_id', Discussion::class, 'id', ['alias' => 'Discussion']);
        $this->belongsTo('user_id', User::class, 'id', ['alias' => 'User']);
    }
}