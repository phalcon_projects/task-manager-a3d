<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class ProjectSettings extends DateTrackingModel
{
    public $id;
    public $project_id;
    public $name;
    public $value;

    public function getSource()
    {
        return 'project_settings';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'project_id' => 'project_id',
                'name' => 'name',
                'value' => 'value'
            ];
    }

    public function initialize()
    {
        $this->hasOne('project_id', Project::class, 'id', ['alias' => 'Project']);
    }
}
