<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class ProjectTeam extends DateTrackingModel
{
    public $id;
    public $project_id;
    public $user_id;
    public $user_role;

    public function getSource()
    {
        return 'project_teams';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'project_id' => 'project_id',
                'user_id' => 'user_id',
                'user_role' => 'user_role'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('project_id', Project::class, 'id', ['alias' => 'Project']);
        $this->belongsTo('user_id', User::class, 'id', ['alias' => 'User']);
        $this->belongsTo('user_role', ProjectUserRole::class, 'id', ['alias' => 'ProjectUserRole']);
    }
}
