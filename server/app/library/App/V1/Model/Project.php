<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Project extends DateTrackingModel
{
    public $id;
    public $title;
    public $description;
    public $creator_id;
    public $state;

    /* region States */
    const STATE_ACTIVE = 1;
    const STATE_SUSPENDED = 2;
    const STATE_CLOSED = 3;
    const STATE_DELETED = 4;

    const STATES = [
        self::STATE_ACTIVE => [
            'id' => self::STATE_ACTIVE,
            'alias' => 'active',
            'title' => 'active',
            'color' => '#f96332'
        ],
        self::STATE_SUSPENDED => [
            'id' => self::STATE_SUSPENDED,
            'alias' => 'suspended',
            'title' => 'Suspended',
            'color' => '#2CA8FF'
        ],
        self::STATE_CLOSED => [
            'id' => self::STATE_CLOSED,
            'alias' => 'closed',
            'title' => 'Closed',
            'color' => '#B8B8B8'
        ],
        self::STATE_DELETED => [
            'id' => self::STATE_DELETED,
            'alias' => 'deleted',
            'title' => 'Deleted',
            'color' => '#B8B8B8'
        ]
    ];
    /* endregion States */

    public function getSource()
    {
        return 'projects';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'title' => 'title',
                'description' => 'description',
                'creator_id' => 'creator_id',
                'state' => 'state',
            ];
    }

    public static function checkUserAccess($projectId, $userId)
    {
        $project = Project::findFirst($projectId);

        if (!$project) {
            return false;
        }

        $user = User::findFirst($userId);

        if (!$user || $user->state != User::STATE_ACTIVE) {
            return false;
        }

        $teamMember = ProjectTeam::findFIrst([
            'conditions' => 'project_id = :1: AND user_id = :2:',
            'bind' => [
                1 => $projectId,
                2 => $userId
            ]
        ]);

        return $teamMember;
    }

    public function initialize()
    {
        $this->hasMany('id', Task::class, 'project_id', ['alias' => 'Tasks']);
        $this->hasMany('creator_id', User::class, 'id', ['alias' => 'Creator']);
        $this->hasManyToMany('id', ProjectTeam::class, 'project_id', 'user_id', User::class, 'id', ['alias' => 'Team']);
    }
}
