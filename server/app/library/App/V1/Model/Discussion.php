<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Discussion extends DateTrackingModel
{
    public $id;
    public $project_id;
    public $creator_id;
    public $title;
    public $description;
    public $state;

    public function getSource()
    {
        return 'discussions';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'project_id' => 'project_id',
                'creator_id' => 'creator_id',
                'title' => 'title',
                'description' => 'description',
                'state' => 'state'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('project_id', Project::class, 'id', ['alias' => 'Project']);
        $this->belongsTo('creator_id', User::class, 'id', ['alias' => 'Creator']);
        $this->hasManyToMany(
            'id',
            DiscussionFollower::class,
            'object_id',
            'user_id',
            User::class,
            'id',
            [
                'alias' => 'Followers',
            ]
        );
    }

    protected function excludedProperties()
    {
        return ['project_id', 'creator_id'];
    }
}
