<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class TimelineSection extends DateTrackingModel
{
    public $id;
    public $timeline_id;
    public $title;
    public $date_to;
    public $state;

    const STATE_ACTIVE = 1;
    const STATE_COMPLETED = 2;

    const STATES = [
        self::STATE_ACTIVE => ['id' => self::STATE_ACTIVE, 'alias' => 'active'],
        self::STATE_COMPLETED => ['id' => self::STATE_COMPLETED, 'alias' => 'completed']
    ];

    public function getSource()
    {
        return 'timeline_sections';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'timeline_id' => 'timeline_id',
                'title' => 'title',
                'date_to' => 'date_to',
                'state' => 'state'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('timeline_id', Timeline::class, 'id', ['alias' => 'Timeline']);
    }

    protected function excludedProperties()
    {
        return [];
    }
}
