<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class ProjectUserRole extends DateTrackingModel
{
    public $id;
    public $title;
    public $alias;
    public $color;

    public function getSource()
    {
        return 'project_user_roles';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'title' => 'title',
                'alias' => 'alias',
                'color' => 'color'
            ];
    }

    public function initialize()
    {
        $this->hasMany('role_id', User::class, 'id', ['alias' => 'User']);
    }
}
