<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class TaskModule extends DateTrackingModel
{
    public $id;
    public $creator_id;
    public $task_id;
    public $type;
    public $source_data;
    public $source_type;

    /* region Types */
    const TYPE_BUTTON = 1;
    const TYPE_VIDEO = 2;
    const TYPE_AUDIO = 3;
    const TYPE_SLIDE_SHOW = 4;
    const TYPE_3D_MODEL = 5;
    const TYPE_360_VIDEO = 6;
    const TYPE_360_IMAGE = 7;
    /* endregion Types */

    public function getSource()
    {
        return 'task_modules';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'creator_id' => 'creator_id',
                'task_id' => 'task_id',
                'type' => 'type',
                'source_data' => 'source_data',
                'source_type' => 'source_type'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('task_id', Task::class, 'id', ['alias' => 'Task']);
        $this->belongsTo('creator_id', User::class, 'id', ['alias' => 'Creator']);
        $this->hasMany('id', Attachment::class, 'object_id', ['alias' => 'Attachments']);
    }

    protected function excludedProperties()
    {
        return ['task_id', 'creator_id'];
    }
}
