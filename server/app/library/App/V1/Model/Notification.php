<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Notification extends DateTrackingModel
{
    public $id;
    public $user_id;
    public $object_id;
    public $object_type;
    public $object_link;
    public $related_user_id;
    public $data;
    public $state;
    public $created_at;
    public $updated_at;

    const STATE_NOT_VIEWED = 0;
    const STATE_VIEWED = 1;

    const STATES = [
        self::STATE_NOT_VIEWED => ['id' => self::STATE_NOT_VIEWED, 'alias' => 'not-viewed'],
        self::STATE_VIEWED => ['id' => self::STATE_VIEWED, 'alias' => 'viewed']
    ];

    public function getSource()
    {
        return 'notifications';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'user_id' => 'user_id',
                'object_id' => 'object_id',
                'object_type' => 'object_type',
                'object_link' => 'object_link',
                'related_user_id' => 'related_user_id',
                'data' => 'data',
                'state' => 'state',
                'created_at' => 'created_at',
                'updated_at' => 'updated_at'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('user_id', User::class, 'id', ['alias' => 'User']);
        $this->belongsTo('related_user_id', User::class, 'id', ['alias' => 'RelatedUser']);
    }

    protected function excludedProperties()
    {
        return ['object_id', 'user_id', 'related_user_id'];
    }
}
