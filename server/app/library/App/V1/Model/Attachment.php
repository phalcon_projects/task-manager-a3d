<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Attachment extends DateTrackingModel
{
    public $id;
    public $title;
    public $description;
    public $path;
    public $type;
    public $creator_id;
    public $object_id;
    public $object_type;
    public $state;

    /* region States */
    const STATE_DELETED = 0;
    const STATE_ACTIVE = 1;

    const STATES = [
        self::STATE_DELETED => [
            'id' => self::STATE_DELETED,
            'alias' => 'deleted',
            'title' => 'Deleted'
        ],
        self::STATE_ACTIVE => [
            'id' => self::STATE_ACTIVE,
            'alias' => 'active',
            'title' => 'Active'
        ]
    ];
    /* endregion States */

    public function getSource()
    {
        return 'attachments';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'title' => 'title',
                'description' => 'description',
                'path' => 'path',
                'type' => 'type',
                'creator_id' => 'creator_id',
                'object_id' => 'object_id',
                'object_type' => 'object_type',
                'state' => 'state'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('creator_id', User::class, 'id', [
            'alias' => 'User',
        ]);
    }

    protected function excludedProperties()
    {
        return ['object_id', 'creator_id'];
    }
}
