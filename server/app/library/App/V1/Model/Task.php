<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Task extends DateTrackingModel
{
    public $id;
    public $title;
    public $description;
    public $project_id;
    public $creator_id;
    public $assignee_id;
    public $state;
    public $sort;
    public $type;
    public $due_date;

    /* region States */
    const STATE_DRAFT = 0;
    const STATE_ACTIVE = 1;
    const STATE_COMPLETED = 2;
    const STATE_DELETED = 3;

    const STATES = [
        self::STATE_DRAFT => [
            'id' => self::STATE_DRAFT,
            'alias' => 'draft',
            'title' => 'Draft',
            'color' => '#f96332'
        ],
        self::STATE_ACTIVE => [
            'id' => self::STATE_ACTIVE,
            'alias' => 'active',
            'title' => 'Active',
            'color' => '#2CA8FF'
        ],
        self::STATE_COMPLETED => [
            'id' => self::STATE_COMPLETED,
            'alias' => 'completed',
            'title' => 'Completed',
            'color' => '#18ce0f'
        ],
        self::STATE_DELETED => [
            'id' => self::STATE_DELETED,
            'alias' => 'deleted',
            'title' => 'Deleted',
            'color' => '#B8B8B8'
        ]
    ];
    /* endregion States */

    /* region Priorities */
    const PRIORITY_DEFAULT = 1;
    const PRIORITY_LOW = 2;
    const PRIORITY_MEDIUM = 3;
    const PRIORITY_HIGH = 4;
    const PRIORITY_CRITICAL = 5;

    const PRIORITIES = [
        self::PRIORITY_DEFAULT => ['id' => self::PRIORITY_DEFAULT, 'title' => 'Default', 'alias' => 'default'],
        self::PRIORITY_LOW => ['id' => self::PRIORITY_LOW, 'title' => 'Low', 'alias' => 'low'],
        self::PRIORITY_MEDIUM => ['id' => self::PRIORITY_MEDIUM, 'title' => 'Medium', 'alias' => 'medium'],
        self::PRIORITY_HIGH => ['id' => self::PRIORITY_HIGH, 'title' => 'High', 'alias' => 'high'],
        self::PRIORITY_CRITICAL => ['id' => self::PRIORITY_CRITICAL, 'title' => 'Critical', 'alias' => 'critical']
    ];
    /* endregion Priorities */

    public function getSource()
    {
        return 'tasks';
    }

    public function columnMap()
    {
        return parent::columnMap() + [
                'id' => 'id',
                'title' => 'title',
                'description' => 'description',
                'project_id' => 'project_id',
                'creator_id' => 'creator_id',
                'assignee_id' => 'assignee_id',
                'primary_id' => 'primary_id',
                'state' => 'state',
                'sort' => 'sort',
                'priority' => 'priority',
                'type' => 'type',
                'due_date' => 'due_date'
            ];
    }

    public function initialize()
    {
        $this->belongsTo('project_id', Project::class, 'id', ['alias' => 'Project']);
        $this->belongsTo('creator_id', User::class, 'id', ['alias' => 'User']);
        $this->belongsTo('assignee_id', User::class, 'id', ['alias' => 'User']);
        $this->belongsTo('primary_id', Task::class, 'id', ['alias' => 'Primary']);
        $this->hasMany('id', Attachment::class, 'object_id', ['alias' => 'Attachments']);
        $this->hasMany('id', Activity::class, 'object_id', ['alias' => 'Activity']);
    }

    protected function excludedProperties()
    {
        return ['project_id', 'creator_id', 'assignee_id', 'primary_id'];
    }
}
