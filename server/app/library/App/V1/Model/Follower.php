<?php

namespace App\V1\Model;

use App\V1\Mvc\DateTrackingModel;

class Follower extends DateTrackingModel
{
    public $user_id;
    public $state;

    public function columnMap()
    {
        return parent::columnMap() + [
                'user_id' => 'user_id',
                'state' => 'state'
            ];
    }
}