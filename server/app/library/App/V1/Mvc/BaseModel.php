<?php

namespace App\V1\Mvc;

use Phalcon\Mvc\Model;

class BaseModel extends Model
{
    /* region States */
    const STATE_DRAFT = 0;
    const STATE_ACTIVE = 1;
    const STATE_COMPLETED = 2;
    const STATE_DELETED = 3;
    /* endregion States */
}
