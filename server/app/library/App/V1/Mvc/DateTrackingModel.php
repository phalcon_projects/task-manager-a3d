<?php

namespace App\V1\Mvc;

class DateTrackingModel extends BaseModel
{
    public $created_at;
    public $updated_at;

    public function columnMap()
    {
        return [
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
        ];
    }

    public function beforeCreate()
    {
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = $this->created_at;
    }

    public function beforeUpdate()
    {
        $this->updated_at = date('Y-m-d H:i:s');
    }
}
