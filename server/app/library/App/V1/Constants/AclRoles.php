<?php

namespace App\V1\Constants;

class AclRoles
{
    const UNAUTHORIZED = '-1';
    const ADMINISTRATOR = '1';
    const USER = '2';

    const AUTHORIZED = [
        self::ADMINISTRATOR,
        self::USER
    ];

    const ALL_ROLES = [
        self::UNAUTHORIZED,
        self::ADMINISTRATOR,
        self::USER
    ];
}