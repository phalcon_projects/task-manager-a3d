<?php

namespace App\V1\Constants;

class Services extends \APP\REST\Constants\Services
{
    const CONFIG = 'config';
    const VIEW = 'view';
    const API_SERVICE = 'api';
}
