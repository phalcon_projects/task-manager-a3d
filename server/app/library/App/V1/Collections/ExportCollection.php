<?php

namespace App\V1\Collections;

use App\V1\Controllers\ExportController;
use APP\REST\Api\ApiCollection;
use APP\REST\Api\ApiEndpoint;

class ExportCollection extends ApiCollection
{
    protected function initialize()
    {
        $this->name('Export')
            ->handler(ExportController::class)
            ->endpoint(ApiEndpoint::get('/documentation.json', 'documentation'))
            ->endpoint(ApiEndpoint::get('/postman.json', 'postman'));
    }
}
