<?php

namespace App\V1\User;

class States
{
    const PENDING = '0';
    const ACTIVE = '1';
    const INACTIVE = '3';
    const DELETED = '4';

    const ALL_STATES = [self::ACTIVE, self::INACTIVE, self::PENDING, self::DELETED];

    const STATES = [
        self::ACTIVE => ['id' => self::ACTIVE, 'alias' => 'active', 'title' => 'Active'],
        self::INACTIVE => ['id' => self::INACTIVE, 'alias' => 'inactive', 'title' => 'Inactive'],
        self::PENDING => ['id' => self::PENDING, 'alias' => 'pending', 'title' => 'Pending'],
        self::DELETED => ['id' => self::DELETED, 'alias' => 'deleted', 'title' => 'Deleted'],
    ];
}