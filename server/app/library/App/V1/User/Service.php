<?php

namespace App\V1\User;

use App\V1\Constants\AclRoles;
use App\V1\Constants\Services;
use App\V1\Model\User;
use PHPMailer\PHPMailer\PHPMailer;

class Service extends \APP\API\User\Service
{
    protected $detailsCache = [];

    public function getRole()
    {
        /** @var User $userModel */
        $userModel = $this->getDetails();

        $role = AclRoles::UNAUTHORIZED;

        if ($userModel && in_array($userModel->role, AclRoles::ALL_ROLES)) {
            $role = $userModel->role;
        }

        return $role;
    }

    public static function generateSecureString($length, $characters)
    {
        $symbols = [];
        $result = [];
        $used_symbols = '';

        $symbols['lower_case'] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols['upper_case'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols['numbers'] = '1234567890';
        $symbols['special_symbols'] = '!?~@#-_+<>[]{}';

        $characters = explode(',', $characters);

        foreach ($characters as $key => $value) {
            $used_symbols .= $symbols[$value];
        }

        $symbols_length = strlen($used_symbols) - 1;

        for ($i = 0; $i < $length; $i++) {
            $n = random_int(0, $symbols_length);
            $result[] = $used_symbols[$n];
        }

        return implode($result);
    }

    protected function getDetailsForIdentity($identity)
    {
        if (array_key_exists($identity, $this->detailsCache)) {
            return $this->detailsCache[$identity];
        }

        $details = User::findFirst((int)$identity);
        $this->detailsCache[$identity] = $details;

        return $details;
    }

    public function sendInvitationEmail($data = [])
    {
        if (!isset($data['email']) || !isset($data['password'])) {
            return false;
        }

        if (!isset($data['subject'])) {
            $data['subject'] = 'Welcome to the ' . Di::getDefault()->getConfig()->clientHostName . '!';
        }

        if (!isset($data['message'])) {
            $data['message'] = 'Greetings' . (isset($data['name']) ? ', ' . $data['name'] : '')
                . '! You invited to ' . Di::getDefault()->getConfig()->application->title . ' system!<br>
<br>We are happy that you join our wonderful crew to create an awesome things!<br>
<br>Go to the <a href="' . Di::getDefault()->getConfig()->clientHostName . '" target="_blank">Awesome website</a> and use provided credentials:<br>
<br>* email: <b>' . $data['email'] . '</b><br>
* password: <b>' . $data['password'] . '</b> to sign and start.<br>
<br>Best regards!';
            $data['plain-text-message'] = 'Greetings' . (isset($data['name']) ? ', ' . $data['name'] : '')
                . '! You invited to ' . Di::getDefault()->getConfig()->application->title . '!
 We are happy that you join our wonderful crew to create an awesome things!
 Go to the "' . Di::getDefault()->getConfig()->clientHostName . '" "Awesome website" and use provided credentials:
 email: ' . $data['email'] . ', password: ' . $data['password'] . ' to sign and start.
 Best regards!';
        }

        return $this->sendEmail($data);
    }

    public function sendInvitationProjectDiscussionEmail($data = [])
    {
        if (!isset($data['email'])) {
            return false;
        }

        if (!isset($data['subject'])) {
            $data['subject'] = 'Welcome to the ' . Di::getDefault()->getConfig()->application->title . '!';
        }

        if (!isset($data['message'])) {
            $data['message'] = 'Greetings' . (isset($data['name']) ? ', ' . $data['name'] : '')
                . '! You invited to ' . Di::getDefault()->getConfig()->application->title . ' system!<br>
<br>We are happy that you join our wonderful crew to create an awesome things!<br>
<br>Go to the <a href="' . Di::getDefault()->getConfig()->clientHostName . '" target="_blank">Awesome website</a> and use provided credentials:<br>
<br>* email: <b>' . $data['email'] . '</b><br>
* password: <b>' . $data['password'] . '</b> to sign and start.<br>
<br>We are so happy and glad to you!';
            $data['plain-text-message'] = 'Greetings' . (isset($data['name']) ? ', ' . $data['name'] : '')
                . '! You invited to ' . Di::getDefault()->getConfig()->application->title . ' system!
 We are happy that you join our wonderful crew to create an awesome things!
 Go to the "' . Di::getDefault()->getConfig()->clientHostName . '" website and use provided credentials:
 email: ' . $data['email'] . ', password: ' . $data['password'] . ' to sign and start.
 Best regards!';
        }

        return $this->sendEmail($data);
    }

    public function sendEmail($data = [])
    {
        if (!isset($data['email']) || !isset($data['subject']) || !isset($data['message'])) {
            return false;
        }

        $config = $this->di->get(Services::CONFIG);

        $mail = $this->initMail();
        $mail->setFrom($config['mail']['from'], $this->di->getDefault()->getConfig()->application->title);
        $mail->addAddress($data['email'], (isset($data['name']) ? $data['name'] : '')); // Add a recipient, Name is optional
        $mail->addReplyTo($config['mail']['from'], $this->di->getDefault()->getConfig()->application->title);
        $mail->addCC($config['mail']['from']);
        $mail->addBCC($config['mail']['from']);

        //$mail->addAttachment('/var/tmp/file.tar.gz'); // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name
        $mail->isHTML(true); // Set email format to HTML

        $mail->Subject = $data['subject'];
        $mail->Body = $data['message'];

        if (isset($data['plain-text-message'])) {
            $mail->AltBody = $data['plain-text-message'];
        }

        if (!$mail->send()) {
            return [
                'success' => false,
                'error' => $mail->ErrorInfo
            ];
        } else {
            return [
                'success' => true
            ];
        }
    }

    protected function initMail()
    {
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3; // Enable verbose debug output
        $mail->isMail(); // Set mailer to use SMTP
        /*$mail->Host = $config['mail']['smtp']['host']; // Specify main and backup SMTP servers
        $mail->Port = $config['mail']['smtp']['port']; // TCP port to connect to
        $mail->SMTPAuth = $config['mail']['smtp']['SMTPAuth']; // Enable SMTP authentication
        $mail->Username = $config['mail']['smtp']['username']; // SMTP username
        $mail->Password = $config['mail']['smtp']['password']; // SMTP password
        $mail->SMTPSecure = $config['mail']['smtp']['SMTPSecure']; // Enable TLS encryption, `ssl` also accepted*/

        return $mail;
    }
}