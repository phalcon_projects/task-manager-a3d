<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\TaskModuleController;
use App\V1\Model\TaskModule;
use App\V1\Transformers\TaskModuleTransformer;

class TaskModuleResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('TaskModule')
            ->model(TaskModule::class)
            ->handler(TaskModuleController::class)
            ->expectsJsonData()
            ->transformer(TaskModuleTransformer::class)
            ->itemKey('task_module')
            ->collectionKey('task_modules')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove());
    }
}
