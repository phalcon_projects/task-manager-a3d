<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\TimelineController;
use App\V1\Model\Timeline;
use App\V1\Transformers\TimelineTransformer;

class TimelineResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Timeline')
            ->model(Timeline::class)
            ->handler(TimelineController::class)
            ->expectsJsonData()
            ->transformer(TimelineTransformer::class)
            ->itemKey('timeline')
            ->collectionKey('timelines')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove())
            ->endpoint(ApiEndpoint::get('/{id}/sections', 'sections'));
    }
}
