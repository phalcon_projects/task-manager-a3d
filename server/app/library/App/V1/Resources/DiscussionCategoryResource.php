<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\DiscussionCategoryController;
use App\V1\Model\DiscussionCategory;
use App\V1\Transformers\DiscussionCategoryTransformer;

class DiscussionCategoryResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Category')
            ->model(DiscussionCategory::class)
            ->handler(DiscussionCategoryController::class)
            ->expectsJsonData()
            ->transformer(DiscussionCategoryTransformer::class)
            ->itemKey('category')
            ->collectionKey('categories')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove());
    }
}
