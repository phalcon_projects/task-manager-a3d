<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\ProjectController;
use App\V1\Model\Project;
use App\V1\Transformers\ProjectTransformer;

class ProjectResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Project')
            ->model(Project::class)
            ->handler(ProjectController::class)
            ->expectsJsonData()
            ->transformer(ProjectTransformer::class)
            ->itemKey('project')
            ->collectionKey('projects')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove())
            ->endpoint(ApiEndpoint::get('/{id}/tasks', 'tasks'))
            ->endpoint(ApiEndpoint::get('/{id}/team', 'team'))
            ->endpoint(ApiEndpoint::post('/{id}/team', 'memberAdd'))
            ->endpoint(ApiEndpoint::put('/{id}/team', 'memberUpdate'))
            ->endpoint(ApiEndpoint::delete('/{id}/team/{team_id}', 'memberRemove'))
            ->endpoint(ApiEndpoint::get('/{id}/team-add', 'membersForAddToTeam'))
            ->endpoint(ApiEndpoint::get('/{id}/settings', 'settingsFetch')
                ->allow(AclRoles::ADMINISTRATOR))
            ->endpoint(ApiEndpoint::post('/{id}/settings', 'settingsSave')
                ->allow(AclRoles::ADMINISTRATOR))
            ->endpoint(ApiEndpoint::get('/{id}/export', 'export')
                ->allow(AclRoles::UNAUTHORIZED))
            ->endpoint(ApiEndpoint::get('/export', 'exportAll')
                ->allow(AclRoles::UNAUTHORIZED));
    }
}
