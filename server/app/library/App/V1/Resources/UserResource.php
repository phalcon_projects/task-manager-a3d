<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiResource;
use APP\REST\Api\ApiEndpoint;
use App\V1\Constants\AclRoles;
use App\V1\Controllers\UserController;
use App\V1\Model\User;
use App\V1\Transformers\UserTransformer;

class UserResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('User')
            ->model(User::class)
            ->expectsJsonData()
            ->transformer(UserTransformer::class)
            ->handler(UserController::class)
            ->itemKey('user')
            ->collectionKey('users')
            ->deny(AclRoles::UNAUTHORIZED)
            ->endpoint(ApiEndpoint::all()
                ->description('Returns all registered users')
            )
            ->endpoint(ApiEndpoint::get('/me', 'me')
                ->deny(AclRoles::UNAUTHORIZED)
                ->description('Returns the currently logged in user')
            )
            ->endpoint(ApiEndpoint::find()
                ->deny(AclRoles::UNAUTHORIZED)
                ->description('Returns the registered user')
            )
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::delete('/delete', 'delete'))
            ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
                ->allow(AclRoles::UNAUTHORIZED)
                ->deny(AclRoles::AUTHORIZED)
                ->description('Authenticates user credentials provided in the authorization header and returns an access token')
                ->exampleResponse([
                    'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
                    'expire' => 1451139067
                ])
            )
            ->endpoint(ApiEndpoint::post('/invite', 'invite'));
    }
}