<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\AttachmentController;
use App\V1\Model\Attachment;
use App\V1\Transformers\AttachmentTransformer;

class AttachmentResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Attachment')
            ->model(Attachment::class)
            ->handler(AttachmentController::class)
            ->expectsPostData()
            ->transformer(AttachmentTransformer::class)
            ->itemKey('attachment')
            ->collectionKey('attachments')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove())
            ->endpoint(ApiEndpoint::post('/change', 'replace')
                ->deny(AclRoles::UNAUTHORIZED));
    }
}
