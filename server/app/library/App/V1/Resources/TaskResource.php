<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\TaskController;
use App\V1\Model\Task;
use App\V1\Transformers\TaskTransformer;

class TaskResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Task')
            ->model(Task::class)
            ->handler(TaskController::class)
            ->expectsJsonData()
            ->transformer(TaskTransformer::class)
            ->itemKey('task')
            ->collectionKey('tasks')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove())
            ->endpoint(ApiEndpoint::get('/{id}/images', 'getImages'))
            ->endpoint(ApiEndpoint::get('/{id}/pdf-image', 'getPDFImage'))
            ->endpoint(ApiEndpoint::put('/sort-replace', 'sortReplace'));
    }
}
