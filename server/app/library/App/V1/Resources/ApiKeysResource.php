<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use App\V1\Controllers\ApiKeyController;
use App\V1\Model\ApiKey;
use App\V1\Transformers\ApiKeyTransformer;

class ApiKeysResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('ApiKeys')
            ->model(ApiKey::class)
            ->handler(ApiKeyController::class)
            ->expectsJsonData()
            ->transformer(ApiKeyTransformer::class)
            ->itemKey('api_key')
            ->collectionKey('api_keys')
            ->deny(AclRoles::UNAUTHORIZED)
            ->deny(AclRoles::USER)
            ->allow(AclRoles::ADMINISTRATOR)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove());
    }
}
