<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\TimelineSectionController;
use App\V1\Model\TimelineSection;
use App\V1\Transformers\TimelineSectionTransformer;

class TimelineSectionResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('TimelineSection')
            ->model(TimelineSection::class)
            ->handler(TimelineSectionController::class)
            ->expectsJsonData()
            ->transformer(TimelineSectionTransformer::class)
            ->itemKey('section')
            ->collectionKey('sections')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove())
            ->endpoint(ApiEndpoint::get('/timeline/{timeline_id}/sections', 'all'));
    }
}
