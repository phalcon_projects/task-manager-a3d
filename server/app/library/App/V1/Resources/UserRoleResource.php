<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiResource;
use APP\REST\Api\ApiEndpoint;
use App\V1\Constants\AclRoles;
use App\V1\Controllers\UserRoleController;
use App\V1\Model\ProjectUserRole;
use App\V1\Transformers\UserRoleTransformer;

class UserRoleResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('ProjectUserRole')
            ->model(ProjectUserRole::class)
            ->expectsJsonData()
            ->transformer(UserRoleTransformer::class)
            ->handler(UserRoleController::class)
            ->itemKey('role')
            ->collectionKey('roles')
            ->deny(AclRoles::UNAUTHORIZED)
            ->endpoint(ApiEndpoint::all()
                ->description('Returns all user roles')
            )
            ->endpoint(ApiEndpoint::find()
                ->description('Returns the user role')
            );
    }
}