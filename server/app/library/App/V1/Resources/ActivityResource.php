<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\ActivityController;
use App\V1\Model\Activity;
use App\V1\Transformers\ActivityTransformer;

class ActivityResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Activity')
            ->model(Activity::class)
            ->handler(ActivityController::class)
            ->expectsJsonData()
            ->transformer(ActivityTransformer::class)
            ->itemKey('activity')
            ->collectionKey('activity')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove());
    }
}
