<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\NotificationController;
use App\V1\Model\Notification;
use App\V1\Transformers\NotificationTransformer;

class NotificationResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Notification')
            ->model(Notification::class)
            ->handler(NotificationController::class)
            ->expectsJsonData()
            ->transformer(NotificationTransformer::class)
            ->itemKey('notification')
            ->collectionKey('notifications')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove());
    }
}
