<?php

namespace App\V1\Resources;

use APP\REST\Api\ApiEndpoint;
use APP\REST\Api\ApiResource;
use App\V1\Constants\AclRoles;
use APP\V1\Controllers\DiscussionController;
use App\V1\Model\Discussion;
use App\V1\Transformers\DiscussionTransformer;

class DiscussionResource extends ApiResource
{
    public function initialize()
    {
        $this
            ->name('Discussion')
            ->model(Discussion::class)
            ->handler(DiscussionController::class)
            ->expectsJsonData()
            ->transformer(DiscussionTransformer::class)
            ->itemKey('discussion')
            ->collectionKey('discussions')
            ->deny(AclRoles::UNAUTHORIZED)

            ->endpoint(ApiEndpoint::all())
            ->endpoint(ApiEndpoint::find())
            ->endpoint(ApiEndpoint::create())
            ->endpoint(ApiEndpoint::update())
            ->endpoint(ApiEndpoint::remove())
            ->endpoint(ApiEndpoint::post('/followers', 'followerAdd')
                ->deny(AclRoles::UNAUTHORIZED))
            ->endpoint(ApiEndpoint::post('/follower-remove', 'followerRemove')
                ->deny(AclRoles::UNAUTHORIZED));
    }
}
