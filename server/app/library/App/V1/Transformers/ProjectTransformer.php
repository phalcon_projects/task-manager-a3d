<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Project;
use App\V1\Model\ProjectSettings;
use App\V1\Model\ProjectTeam;
use App\V1\Model\ProjectUserRole;
use App\V1\Model\Task;
use App\V1\Model\User;

class ProjectTransformer extends ModelTransformer
{
    protected $modelClass = Project::class;

    protected $availableIncludes = [
        'creator',
        'tasks',
        'team',
        'reviews_settings'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $projectUserRoles = ProjectUserRole::find();
        $projectUserRolesData = [];

        foreach ($projectUserRoles as $projectUserRoleItem) {
            $projectUserRolesData[$projectUserRoleItem->id] = $projectUserRoleItem->toArray();
        }

        $team = ProjectTeam::find([
            'conditions' => 'project_id = :1:',
            'bind' => [
                1 => $item->id
            ]
        ]);
        $teamData = [];

        foreach ($team as $teamMember) {
            $userData = User::findFirst([
                'conditions' => 'id = ?1 AND state = ?2',
                'bind' => [
                    1 => $teamMember->user_id,
                    2 => User::STATE_ACTIVE
                ]
            ]);

            if ($userData) {
                $user = (new UserTransformer())->transform($userData, ['projects']) ?: null;

                if ($user) {
                    $teamData[] = [
                        'id' => $teamMember->id,
                        'user_id' => $user['id'],
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'avatar' => $user['avatar'],
                        'role' => $projectUserRolesData[$teamMember->user_role]
                    ];
                }
            }
        }

        $creator = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->creator_id,
            ]
        ]);

        $taskList = [];
        $tasks = Task::find([
            'project_id = :projectId: AND primary_id = 0 AND (state = :stateActive: OR state = :stateCompleted:)',
            'bind' => [
                'projectId' => $item->id,
                'stateActive' => Task::STATE_ACTIVE,
                'stateCompleted' => Task::STATE_COMPLETED
            ]
        ]);

        foreach ($tasks as $taskItem) {
            $taskList[] = (new TaskTransformer())->transform($taskItem, 'project');
        }

        $hasAppReviewsSettings = !!ProjectSettings::findFirst([
            'project_id = :project_id:',
            'bind' => [
                'project_id' => $item->id
            ]
        ]);

        $data = [
            'state' => isset(Project::STATES[$item->state]) ? Project::STATES[$item->state] : null,
            'creator' => (new UserTransformer())->transform($creator, ['projects']) ?: null,
            'tasks' => $taskList,
            'team' => $teamData,
            'reviews_settings' => $hasAppReviewsSettings
        ];

        return $data;
    }

    public function transform($project, $excludedProperties = null)
    {
        $data = [
            'id' => $this->int($project->id),
            'title' => $project->title,
            'description' => $project->description,
            'creator_id' => $project->updated_at,
            'state' => $project->updated_at
        ];

        $combinedResult = array_merge($data, $this->additionalFields($project, $excludedProperties));

        return $combinedResult;
    }
}
