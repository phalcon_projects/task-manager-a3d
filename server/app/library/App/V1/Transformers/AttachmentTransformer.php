<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Attachment;
use App\V1\Model\Task;
use App\V1\Model\User;

class AttachmentTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = Attachment::class;

    protected $availableIncludes = [
        'object',
        'creator'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $data = [];

        if ($item->type == 'task') {
            $data['object'] = Task::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $item->object_id,
                ]
            ]) ?: null;
        }

        $data = [
            'creator' => User::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $item->creator_id,
                ]
            ]) ?: null
        ];
        return $data;
    }
}
