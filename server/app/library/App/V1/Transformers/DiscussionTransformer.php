<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Activity;
use App\V1\Model\Attachment;
use App\V1\Model\Discussion;
use App\V1\Model\Project;
use App\V1\Model\User;

class DiscussionTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = Discussion::class;

    protected $availableIncludes = [
        'project',
        'creator',
        'state',
        'activities',
        'followers',
        'attachments'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $activities = Activity::find([
            'conditions' => 'object_id = ?1 AND (type = "comment" AND (state = ?2 OR state = ?3) OR (type = "attachment" AND state = ?2)) AND object_type = "discussion"',
            'bind' => [
                1 => $item->id,
                2 => Activity::STATE_ACTIVE,
                3 => Activity::STATE_DRAFT
            ],
            'order' => 'created_at ASC'
        ]) ?: null;

        $preparedActivitiesData = [];

        foreach ($activities as $activityItem) {
            $preparedActivitiesData[] = (new ActivityTransformer())->transform($activityItem);
        }

        $followers = [];

        foreach ($item->followers as $follower) {
            unset($follower->password);
            $followers[] = (new UserTransformer())->transform($follower, ['projects']);
        }

        $creator = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->creator_id,
            ]
        ]) ?: null;

        if (isset($creator->password)) {
            unset($creator->password);
        }

        $project = Project::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->project_id,
            ]
        ]);

        $data = [
            'creator' => (new UserTransformer())->transform($creator, ['projects']),
            'project' => $project ?: null,
            'activities' => $preparedActivitiesData,
            'followers' => $followers,
            'attachments' => Attachment::find([
                'conditions' => 'object_id = ?1 AND object_type = "discussion" AND state = 1',
                'bind' => [
                    1 => $item->id,
                ]
            ]) ?: null
        ];

        return $data;
    }
}
