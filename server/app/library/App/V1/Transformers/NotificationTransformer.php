<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Notification;
use App\V1\Model\Project;
use App\V1\Model\Task;
use App\V1\Model\Discussion;
use App\V1\Model\User;

class NotificationTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = Notification::class;

    protected $availableIncludes = [
        'user',
        'object',
        'related_user',
        'state'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $object = null;
        $objectModelClass = 'App\\V1\\Model\\' . ucfirst($item->object_type);

        if (class_exists($objectModelClass)) {
            $object = $objectModelClass::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $item->object_id,
                ]
            ]);

            if ($object) {
                $objectTransformerClass = $objectModelClass . 'Transformer';
                $object = class_exists($objectTransformerClass)
                    ? (new $objectTransformerClass())->transform($object)
                    : $object->toArray();
            } else {
                $object = null;
            }
        }

        $user = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->user_id,
            ]
        ]);
        $notifiedUser = $user ? (new UserTransformer())->transform($user, ['projects']) : null;

        if ($notifiedUser && isset($notifiedUser['projects'])) {
            unset($notifiedUser['projects']);
        }

        $user = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->related_user_id,
            ]
        ]);
        $notifierUser = $user ? (new UserTransformer())->transform($user, ['projects']) : null;

        if ($notifierUser && isset($notifierUser['projects'])) {
            unset($notifierUser['projects']);
        }

        $data = [
            'user' => $notifiedUser,
            'object' => $object,
            'related_user' => $notifierUser,
            'state' => Notification::STATES[$item->state]
        ];

        return $data;
    }
}
