<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Activity;
use App\V1\Model\Project;
use App\V1\Model\Task;
use App\V1\Model\Discussion;
use App\V1\Model\User;

class ActivityTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = Activity::class;

    protected $availableIncludes = [
        'object',
        'related',
        'creator',
        'project',
        'state'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $object = null;
        $objectModelClass = 'App\\V1\\Model\\' . ucfirst($item->object_type);

        if (class_exists($objectModelClass)) {
            $object = $objectModelClass::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $item->object_id,
                ]
            ]);

            if ($object) {
                $objectTransformerClass = $objectModelClass . 'Transformer';
                $object = class_exists($objectTransformerClass)
                    ? (new $objectTransformerClass())->transform($object)
                    : $object->toArray();
            } else {
                $object = null;
            }
        }

        $user = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->creator_id,
            ]
        ]);
        $creator = $user ? (new UserTransformer())->transform($user, ['projects']) : null;

        if ($creator && isset($creator['projects'])) {
            unset($creator['projects']);
        }

        $data = [
            'object' => $object,
            'creator' => $creator,
            'project' => Project::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $item->project_id,
                ]
            ]) ?: null,
            'state' => Activity::STATES[$item->state]
        ];

        return $data;
    }
}
