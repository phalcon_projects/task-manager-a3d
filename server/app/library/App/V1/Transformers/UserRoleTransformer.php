<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\ProjectUserRole;

class UserRoleTransformer extends ModelTransformer
{
    protected $modelClass = ProjectUserRole::class;
}