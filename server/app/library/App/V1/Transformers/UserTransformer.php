<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Attachment;
use App\V1\Model\Project;
use App\V1\Model\ProjectTeam;
use App\V1\Model\User;
use App\V1\Model\ProjectUserRole;
use App\V1\User\States;

class UserTransformer extends ModelTransformer
{
    protected $modelClass = User::class;

    protected $availableIncludes = [
        'avatar',
        'role',
        'state'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $avatar = Attachment::findFirst([
            'conditions' => 'creator_id = ?1 AND object_type = "user" AND state = 1',
            'bind' => [
                1 => $item->id,
            ],
            'order' => 'created_at DESC'
        ]) ?: null;

        $data = [
            'avatar' => $avatar,
            'role' => User::USER_ROLES[$item->role],
        ];

        return $data;
    }

    public function includeRole($item)
    {
        return $this->item($item, new UserRoleTransformer());
    }

    protected function excludedProperties()
    {
        return ['password'];
    }

    public function transform($user, $excludedProperties = null)
    {
        $data = [
            'id' => $this->int($user->id),
            'login' => $user->login,
            'email' => $user->email,
            'name' => $user->name,
            'state' => isset(States::STATES[$user->state]) ? States::STATES[$user->state] : null,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at
        ];

        $combinedResult = array_merge($data, $this->additionalFields($user, $excludedProperties));

        return $combinedResult;
    }
}