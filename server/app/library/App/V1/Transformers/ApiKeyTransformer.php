<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\ApiKey;

class ApiKeyTransformer extends ModelTransformer
{
    protected $modelClass = ApiKey::class;
}
