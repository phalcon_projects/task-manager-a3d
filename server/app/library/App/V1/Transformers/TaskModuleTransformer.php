<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Attachment;
use App\V1\Model\Task;
use App\V1\Model\TaskModule;
use App\V1\Model\User;

class TaskModuleTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = TaskModule::class;

    protected $availableIncludes = [
        'attachments'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $creator = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->creator_id,
            ]
        ]);

        $data = [
            'creator' => $creator ? (new UserTransformer())->transform($creator, ['projects']) : null,
            'attachments' => Attachment::find([
                'conditions' => 'object_id = ?1 AND object_type = "module" AND state = 1',
                'bind' => [
                    1 => $item->id,
                ]
            ]) ?: null
        ];
        return $data;
    }

    public function includeProject(Task $task)
    {
        return $this->item($task->getProject(), new ProjectTransformer());
    }

    /**
     * Transform manually by using
     * the following code (below):
     *
     * public function transform(Task $task)
     * {
     * return [
     * 'id' => $this->int($task->id),
     * 'title' => $task->title,
     * 'project_id' => $this->int($task->project_id)
     * ];
     * }
     */
}
