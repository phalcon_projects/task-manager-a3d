<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Timeline;
use App\V1\Model\TimelineSection;

class TimelineTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = Timeline::class;

    protected $availableIncludes = [
        'object',
        'sections'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $object = null;
        $objectModelClass = 'App\\V1\\Model\\' . ucfirst($item->object_type);

        if (class_exists($objectModelClass)) {
            $object = $objectModelClass::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [1 => $item->object_id]
            ]);

            if ($object) {
                $objectTransformerClass = $objectModelClass . 'Transformer';
                $object = class_exists($objectTransformerClass)
                    ? (new $objectTransformerClass())->transform($object)
                    : $object->toArray();
            } else {
                $object = null;
            }
        }

        $sectionsArray = TimelineSection::find([
            'conditions' => 'timeline_id = ?1',
            'bind' => [1 => $item->id]
        ]);
        $sections = [];

        if ($sectionsArray) {
            foreach ($sectionsArray as $sectionItem) {
                $sections[] = (new TimelineSectionTransformer())->transform($sectionItem);
            }
        }

        $data = [
            'object' => $object,
            'sections' => $sections
        ];

        return $data;
    }
}
