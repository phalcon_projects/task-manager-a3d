<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\TimelineSection;

class TimelineSectionTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = TimelineSection::class;

    protected $availableIncludes = [];

    protected function additionalFields($item, $excludedProperties = null)
    {
        return [];
    }

    /**
     * Transform manually by using
     * the following code (below):
     *
     * public function transform(Task $task)
     * {
     * return [
     * 'id' => $this->int($task->id),
     * 'title' => $task->title,
     * 'project_id' => $this->int($task->project_id)
     * ];
     * }
     */
}
