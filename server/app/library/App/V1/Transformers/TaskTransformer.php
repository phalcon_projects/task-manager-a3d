<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\Attachment;
use App\V1\Model\Project;
use App\V1\Model\Task;
use App\V1\Model\Activity;
use App\V1\Model\TaskModule;
use App\V1\Model\Timeline;
use App\V1\Model\User;

class TaskTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = Task::class;

    protected $availableIncludes = [
        'project',
        'creator',
        'assignee',
        'state',
        'type',
        'primary',
        'attachments',
        'activities',
        'modules',
        'timeline'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $timeline = null;

        if ($item->primary_id === '0') {
            $timelineData = Timeline::findFirst([
                'object_id = :1: AND object_type = "task"',
                'bind' => [
                    1 => $item->id
                ]
            ]);

            if ($timelineData) {
                $timeline = (new TimelineTransformer())->transform($timelineData);
            }
        }

        $activities = Activity::find([
            'conditions' => 'object_id = ?1 AND (type = "comment" AND (state = ?2 OR state = ?3) OR (type = "attachment" AND state = ?2) OR type = "task") AND object_type = "task"',
            'bind' => [
                1 => $item->id,
                2 => Activity::STATE_ACTIVE,
                3 => Activity::STATE_DRAFT
            ],
            'order' => 'created_at ASC'
        ]) ?: null;

        $preparedActivitiesData = [];

        foreach ($activities as $activity) {
            $preparedActivitiesData[] = (new ActivityTransformer())->transform($activity);
        }

        $subTasks = Task::find([
            'conditions' => 'primary_id = :primary_id: AND (state = :state_active: OR state = :state_completed:)',
            'bind' => [
                'primary_id' => $item->id,
                'state_active' => Task::STATE_ACTIVE,
                'state_completed' => Task::STATE_COMPLETED
            ],
            'order' => 'sort'
        ]) ?: null;

        $preparedSubTasksData = [];

        foreach ($subTasks as $subTaskItem) {
            if ($subTaskItem->id == $item->id) {
                continue;
            }

            $preparedSubTasksData[] = (new TaskTransformer())->transform($subTaskItem);
        }

        $project = Project::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->project_id,
            ]
        ]);
        $creator = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->creator_id,
            ]
        ]);
        $assignee = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $item->assignee_id,
            ]
        ]);
        $modules = TaskModule::find([
            'conditions' => 'task_id = ?1',
            'bind' => [
                1 => $item->id
            ],
            'order' => 'created_at ASC'
        ]) ?: null;

        $preparedModulesData = [];

        foreach ($modules as $module) {
            $preparedModulesData[] = (new TaskModuleTransformer())->transform($module);
        }

        $data = [
            'project' => $project ?: null,
            'creator' => $creator ? (new UserTransformer())->transform($creator, ['projects']) : null,
            'assignee' => $assignee ? (new UserTransformer())->transform($assignee, ['projects']) : null,
            'state' => isset(Task::STATES[$item->state]) ? Task::STATES[$item->state] : null,
            'priority' => isset(Task::PRIORITIES[$item->priority]) ? Task::PRIORITIES[$item->priority] : null,
            'primary' => Task::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $item->primary_id,
                ]
            ]) ?: null,
            'sub_tasks' => $preparedSubTasksData,
            'activities' => $preparedActivitiesData,
            'attachments' => Attachment::find([
                'conditions' => 'object_id = :object_id: AND object_type = :object_type: AND state = :state:',
                'bind' => [
                    'object_id' => $item->id,
                    'object_type' => 'task',
                    'state' => 1
                ]
            ]) ?: null,
            'modules' => $preparedModulesData,
            'timeline' => $timeline
        ];
        return $data;
    }

    /**
     * Transform manually by using
     * the following code (below):
     *
     * public function transform(Task $task)
     * {
     * return [
     * 'id' => $this->int($task->id),
     * 'title' => $task->title,
     * 'project_id' => $this->int($task->project_id)
     * ];
     * }
     */
}
