<?php

namespace App\V1\Transformers;

use APP\REST\Transformers\ModelTransformer;
use App\V1\Model\DiscussionCategory;
use App\V1\Model\User;

class DiscussionCategoryTransformer extends ModelTransformer
{
    /**
     * Transforms are automatically handled
     * based on your model when you extend ModelTransformer
     * and assign the modelClass property
     */
    protected $modelClass = DiscussionCategory::class;

    protected $availableIncludes = [
        'author'
    ];

    protected function additionalFields($item, $excludedProperties = null)
    {
        $data = [
            'author' => User::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $item->creator_id,
                ]
            ]) ?: null
        ];
        return $data;
    }
}
