<?php

namespace App\V1\Bootstrap;

use APP\API\Exception;
use APP\REST\Api;
use App\V1\BootstrapInterface;
use App\V1\Collections\ExportCollection;
use App\V1\Resources\ActivityResource;
use App\V1\Resources\ApiKeysResource;
use App\V1\Resources\AttachmentResource;
use App\V1\Resources\DiscussionCategoryResource;
use App\V1\Resources\DiscussionResource;
use App\V1\Resources\NotificationResource;
use App\V1\Resources\ProjectResource;
use App\V1\Resources\TaskResource;
use App\V1\Resources\TaskModuleResource;
use App\V1\Resources\TimelineResource;
use App\V1\Resources\TimelineSectionResource;
use App\V1\Resources\UserResource;
use App\V1\Resources\UserRoleResource;
use Phalcon\Config;
use Phalcon\DiInterface;

class CollectionBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        $apiVersionPrefix = 'api/' . $config->api->version;

        try {
            $api->collection(new ExportCollection("/{$apiVersionPrefix}/export"))
                ->resource(new UserResource("/{$apiVersionPrefix}/users"))
                ->resource(new UserRoleResource("/{$apiVersionPrefix}/users/roles"))
                ->resource(new ProjectResource("/{$apiVersionPrefix}/projects"))
                ->resource(new TaskResource("/{$apiVersionPrefix}/tasks"))
                ->resource(new TimelineResource("/{$apiVersionPrefix}/timeline"))
                ->resource(new TimelineSectionResource("/{$apiVersionPrefix}/timeline/sections"))
                ->resource(new TaskModuleResource("/{$apiVersionPrefix}/task-modules"))
                ->resource(new DiscussionResource("/{$apiVersionPrefix}/discussions"))
                ->resource(new DiscussionCategoryResource("/{$apiVersionPrefix}/discussions/categories"))
                ->resource(new ActivityResource("/{$apiVersionPrefix}/activities"))
                ->resource(new NotificationResource("/{$apiVersionPrefix}/notifications"))
                ->resource(new AttachmentResource("/{$apiVersionPrefix}/attachments"))
                ->resource(new ApiKeysResource("/{$apiVersionPrefix}/api-keys"));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
