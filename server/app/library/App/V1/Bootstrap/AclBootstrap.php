<?php

namespace App\V1\Bootstrap;

use APP\API\Acl\MountingEnabledAdapterInterface;
use APP\REST\Api;
use App\V1\BootstrapInterface;
use App\V1\Constants\Services;
use App\V1\Constants\AclRoles;
use Phalcon\Acl;
use Phalcon\Config;
use Phalcon\DiInterface;

class AclBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        /** @var MountingEnabledAdapterInterface $acl */
        $acl = $di->get(Services::ACL);

        $unauthorizedRole = new Acl\Role(AclRoles::UNAUTHORIZED);

        $acl->addRole($unauthorizedRole);

        $acl->addRole(new Acl\Role(AclRoles::ADMINISTRATOR));
        $acl->addRole(new Acl\Role(AclRoles::USER));

        $acl->mountMany($api->getCollections());
    }
}