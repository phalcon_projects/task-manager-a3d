<?php

namespace App\V1\Bootstrap;

use APP\REST\Api;
use App\V1\BootstrapInterface;
use App\V1\Constants\Services;
use Phalcon\Config;
use Phalcon\DiInterface;
use Phalcon\Mvc\View\Simple;

class RouteBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        $api->get('/', function () use ($api) {

            /** @var Simple $view */
            $view = $api->di->get(Services::VIEW);

            return $view->render('general/index');
        });

        $api->get('/proxy.html', function () use ($api, $config) {

            /** @var Simple $view */
            $view = $api->di->get(Services::VIEW);

            $view->setVar('client', $config->clientHostName);
            return $view->render('general/proxy');
        });

        $api->get('/documentation.html', function () use ($api, $config) {

            /** @var Simple $view */
            $view = $api->di->get(Services::VIEW);

            $view->setVar('title', $config->application->title);
            $view->setVar('description', $config->application->description);
            $view->setVar('documentationPath', $config->hostName . '/export/documentation.json');
            return $view->render('general/documentation');
        });
    }
}