<?php

namespace App\V1\Bootstrap;

use App\V1\BootstrapInterface;
use Phalcon\Config;
use Phalcon\DiInterface;
use APP\REST\Api;
use APP\API\Middleware\AuthenticationMiddleware;
use APP\REST\Middleware\AuthorizationMiddleware;
use APP\API\Middleware\CorsMiddleware;
use APP\REST\Middleware\FractalMiddleware;
use APP\API\Middleware\NotFoundMiddleware;
use APP\API\Middleware\OptionsResponseMiddleware;
use APP\API\Middleware\UrlQueryMiddleware;

class MiddlewareBootstrap implements BootstrapInterface
{
    public function run(Api $api, DiInterface $di, Config $config)
    {
        $api
            ->attach(new CorsMiddleware(
                $config->cors->allowedOrigins->toArray(),
                $config->cors->allowedMethods->toArray(),
                $config->cors->allowedHeaders->toArray())
            )
            ->attach(new OptionsResponseMiddleware)
            ->attach(new NotFoundMiddleware)
            ->attach(new AuthenticationMiddleware)
            ->attach(new AuthorizationMiddleware)
            ->attach(new FractalMiddleware)
            ->attach(new UrlQueryMiddleware);
    }
}