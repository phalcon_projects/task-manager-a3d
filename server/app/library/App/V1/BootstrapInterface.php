<?php

namespace App\V1;

use Phalcon\Config;
use Phalcon\DiInterface;
use APP\REST\Api;

interface BootstrapInterface {

    public function run(Api $api, DiInterface $di, Config $config);

}