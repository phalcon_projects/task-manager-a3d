<?php

namespace App\V1\Controllers;

use APP\REST\Mvc\Controllers\CrudResourceController;
use Phalcon\Mvc\Model as Model;

class TimelineSectionController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'id',
            'timeline_id',
            'title',
            'date_to',
            'state'
        ];
    }

    protected function afterHandleRemove(Model $removedItem, $response)
    {
        if (!$removedItem->delete()) {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Error item remove.');
        }
    }
}
