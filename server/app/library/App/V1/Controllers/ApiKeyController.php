<?php

namespace App\V1\Controllers;

use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\User\Service;

class ApiKeyController extends CrudResourceController
{
    protected $isCreation = false;

    public function whitelist()
    {
        return [
            'value',
            'state'
        ];
    }

    protected function getPostedData()
    {
        if ($this->isCreation) {
            $this->isCreation = false;
            $keyValue = Service::generateSecureString(64, 'lower_case,upper_case,numbers');

            return [
                'value' => $keyValue
            ];
        } else {
            return parent::getPostedData();
        }
    }

    public function create()
    {
        $this->isCreation = true;
        return parent::create();
    }
}
