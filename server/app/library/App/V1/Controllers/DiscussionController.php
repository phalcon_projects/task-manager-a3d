<?php

namespace App\V1\Controllers;

use APP\API\Constants\ErrorCodes;
use APP\API\Exception;
use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\Model\Discussion;
use App\V1\Model\DiscussionFollower;
use App\V1\Model\Project;
use App\V1\Model\User;
use App\V1\Transformers\DiscussionTransformer;

class DiscussionController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'project_id',
            'creator_id',
            'title',
            'description',
            'updated_at'
        ];
    }

    public function getPostedData()
    {
        $data = parent::getPostedData();
        $user = $this->getUser();

        if ($user && !isset($data['creator_id'])) {
            $data['creator_id'] = $user->id;
        }

        return $data;
    }

    public function create()
    {
        $response = parent::create();
        $discussionFollower = new DiscussionFollower();
        $discussionFollower->object_id = $response['discussion']['id'];
        $user = $this->getUser();

        if ($user) {
            $discussionFollower->user_id = $user->id;
        }

        if ($discussionFollower->save()) {
            $response['discussion']['followers'][] = $response['discussion']['creator'];
        }

        return $response;
    }

    public function followerRemove()
    {
        $data = $this->request->getPostedData();

        if (!isset($data['user_id']) || !isset($data['object_id'])) {
            $this->onDataInvalid($data);
        }

        if (!DiscussionFollower::findFirst([
            'columns' => 'user_id, object_id',
            'conditions' => 'user_id = ?1 AND object_id = ?2 AND object_type = "discussion"',
            'bind' => [
                1 => $data['user_id'],
                2 => $data['object_id']
            ]])) {
            throw new Exception(
                ErrorCodes::POST_DATA_INVALID,
                'Member is not in discussion followers list',
                ['data' => $data]
            );
        }

        if (!$user = User::findFirst([
                'columns' => 'id',
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $data['user_id'],
                ]
            ]) || !$discussion = Discussion::findFirst([
                'columns' => 'id',
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $data['object_id'],
                ]
            ])) {
            $this->onDataInvalid($data);
        }

        $discussionFollower = new DiscussionFollower();
        $discussionFollower->user_id = $data['user_id'];
        $discussionFollower->object_id = $data['object_id'];

        if (!$discussionFollower->delete()) {
            return $this->onRemoveFailed($discussionFollower);
        }

        return ['status' => 'OK'];
    }

    public function followerAdd()
    {
        $data = $this->request->getPostedData();

        if (!isset($data['user_id']) || !isset($data['object_id'])) {
            $this->onDataInvalid($data);
        }

        if (DiscussionFollower::findFirst([
            'columns' => 'user_id, object_id',
            'conditions' => 'user_id = ?1 AND object_id = ?2 AND object_type = "discussion"',
            'bind' => [
                1 => $data['user_id'],
                2 => $data['object_id']
            ]])) {
            throw new Exception(
                ErrorCodes::POST_DATA_INVALID,
                'Member already is in discussion followers list',
                ['data' => $data]
            );
        }

        $user = null;

        if (!$user = User::findFirst([
                'columns' => 'id',
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $data['user_id'],
                ]
            ]) || !$discussion = Discussion::findFirst([
                'columns' => 'id',
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $data['object_id'],
                ]
            ])) {
            $this->onDataInvalid($data);
        }

        $discussionFollower = new DiscussionFollower();
        $discussionFollower->user_id = $data['user_id'];
        $discussionFollower->object_id = $data['object_id'];

        if (!$discussionFollower->save()) {
            return $this->onCreateFailed($discussionFollower, $data);
        }

        $transformer = new DiscussionTransformer();
        $transformer->setModelClass(Discussion::class);

        $discussion = $this->createItemResponse(Discussion::findFirst($data['object_id']), $transformer);

        $project = Project::findFirst($discussion->project_id);

        if ($user && $discussion) {
            $followersResponse = $discussion['followers'];
        } else {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Unable to get item', [
                'messages' => 'Unable to fetch discussion',
                'data' => $data,
                'item' => $discussion->toArray()
            ]);
        }

        if ($followersResponse) {
            $emailSentResult = $this->sendInvitationEmail(array_merge($data, [
                'email' => $user->email,
                'project_title' => $project
            ]));

            if (!isset($emailSentResult['success']) || !$emailSentResult['success']) {
                throw new Exception(ErrorCodes::DATA_FAILED, 'Unable to send invitation email', [
                    'messages' => $emailSentResult['error'],
                    'data' => $data,
                    'item' => $user->toArray()
                ]);
            }
        }

        $response = [
            'followers' => $followersResponse
        ];

        return $response;
    }
}
