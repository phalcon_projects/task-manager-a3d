<?php

namespace App\V1\Controllers;

use APP\API\Constants\ErrorCodes;
use APP\API\Constants\PostedDataMethods;
use APP\API\Exception;
use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\Constants\AclRoles;
use App\V1\Constants\Services;
use App\V1\Model\ApiKey;
use App\V1\Model\Project;
use App\V1\Model\ProjectSettings;
use App\V1\Model\ProjectTeam;
use App\V1\Model\ProjectUserRole;
use App\V1\Model\Task;
use App\V1\Model\User;
use App\V1\Transformers\ProjectTransformer;
use App\V1\Transformers\TaskTransformer;
use App\V1\Transformers\UserTransformer;
use Phalcon\Mvc\Model as Model;

class ProjectController extends CrudResourceController
{
    protected function afterHandleAll($data, &$response)
    {
        foreach ($response['projects'] as $id => $project) {
            if (!Project::checkUserAccess($project['id'], $this->getUser()->id)) {
                unset($response['projects'][$id]);
            }
        }
    }

    public function tasks($id)
    {
        $list = Task::find([
            'project_id = :1: AND primary_id = 0 AND state != :2:',
            'bind' => [
                1 => $id,
                2 => Task::STATE_DELETED
            ]
        ]);

        $result = [];

        foreach ($list as $listItem) {
            $result[] = (new TaskTransformer())->transform($listItem);
        }

        $response['tasks'] = $result;

        return $response;
    }

    protected function getPostedData()
    {
        $data = parent::getPostedData();
        $user = $this->getUser();

        if ($user && !isset($data['creator_id'])) {
            $data['creator_id'] = $user->id;
        }

        return $data;
    }

    protected function afterHandleCreate(Model $createdItem, $data, $response)
    {
        try {
            $newProjectTeamItem = new ProjectTeam();
            $newProjectTeamItem->project_id = $createdItem->id;
            $newProjectTeamItem->user_id = $createdItem->creator_id;
            $administratorRole = ProjectUserRole::findFirst([
                'conditions' => 'alias = ?1',
                'bind' => [
                    1 => User::USER_ROLES[AclRoles::ADMINISTRATOR]['alias']
                ]
            ]);
            $newProjectTeamItem->user_role = $administratorRole->id;

            if (!$newProjectTeamItem->save()) {
                $this->onCreateFailed($newProjectTeamItem, $createdItem);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function memberAdd($projectId)
    {
        $data = $this->request->getPostedData(PostedDataMethods::JSON_BODY);

        if (!$projectId || !isset($data['user_id']) || !isset($data['user_role'])) {
            $this->onDataInvalid($data);
        }

        $data['project_id'] = $projectId;

        if (ProjectTeam::findFirst([
            'columns' => 'user_id, project_id, user_role',
            'conditions' => 'user_id = ?1 AND project_id = ?2',
            'bind' => [
                1 => $data['user_id'],
                2 => $data['project_id']
            ]])
        ) {
            throw new Exception(
                ErrorCodes::POST_DATA_INVALID,
                'Member already is in project team',
                ['data' => $data]
            );
        }

        $user = User::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $data['user_id'],
            ]
        ]) ?: null;

        $project = Project::findFirst([
            'columns' => 'id',
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $data['project_id'],
            ]
        ]) ?: null;

        if (!$user || !$project) {
            $this->onDataInvalid($data);
        }

        $teamMember = new ProjectTeam([
            'user_id' => $data['user_id'],
            'project_id' => $data['project_id'],
            'user_role' => $data['user_role']
        ]);

        if (!$createResult = $teamMember->save()) {
            return $this->onCreateFailed($teamMember, $data);
        }

        $project = (new ProjectTransformer())->transform(Project::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $data['project_id'],
            ]
        ]));

        if ($project) {
            $teamResponse = $project['team'];
        } else {
            return new Exception(ErrorCodes::DATA_FAILED, 'Unable to get item', [
                'messages' => 'Unable to fetch project',
                'data' => $data,
                'item' => $project->toArray()
            ]);
        }

        $response = [
            'members' => $teamResponse
        ];

        if ($teamResponse) {
            $emailData = [];
            $emailData['email'] = $user->email;

            $emailData['subject'] = 'You invited the project';
            $config = $this->di->get(Services::CONFIG);
            $emailData['message'] = 'Greetings' . ($user->name ? ', ' . $user->name : '')
                . '! <br>You invited to the project <a href="' . $config['hostName'] . '" target="_blank">'
                . $project['title'] . '</a> at ' . $this->di->getConfig()->application->title . ' system!';
            $emailSentResult = $this->userService->sendEmail($emailData);

            if (!isset($emailSentResult['success']) || !$emailSentResult['success']) {
                $response['email'] = [
                    'code' => ErrorCodes::MAIL_SERVER_ERROR,
                    'messages' => 'Unable to send invitation email',
                    'data' => $data,
                    'item' => $user->toArray()
                ];
            }
        }

        return $response;
    }

    public function memberRemove($projectId, $teamId)
    {
        $teamMember = ProjectTeam::findFirst($teamId);
        $data = [
            'project_id' => $projectId,
            'team_id' => $teamId
        ];

        if (!$teamMember || !$project = Project::findFirst([
                'columns' => 'id',
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $projectId,
                ]
            ])
        ) {
            return $this->onDataInvalid($data);
        }

        if (!$this->removeItem($teamMember)) {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Data remove is failed', ['data' => $data]);
        }

        $response = [
            'success' => true,
            'message' => 'Item was removed'
        ];

        return $this->createArrayResponse($response, 'data');
    }

    public function memberUpdate($projectId)
    {
        $data = $this->request->getPostedData(PostedDataMethods::JSON_BODY);

        if (!$projectId || !isset($data['user_id']) || !isset($data['user_role'])) {
            return $this->onDataInvalid($data);
        }

        $data['project_id'] = $projectId;
        $teamMember = ProjectTeam::findFirst([
            'conditions' => 'user_id = ?1 AND project_id = ?2',
            'bind' => [
                1 => $data['user_id'],
                2 => $data['project_id']
            ]]);

        if (!$teamMember || !$user = User::findFirst([
                    'columns' => 'id',
                    'conditions' => 'id = ?1',
                    'bind' => [
                        1 => $data['user_id'],
                    ]
                ]) || !$project = Project::findFirst([
                    'columns' => 'id',
                    'conditions' => 'id = ?1',
                    'bind' => [
                        1 => $data['project_id'],
                    ]
                ])
        ) {
            return $this->onDataInvalid($data);
        }

        $teamMember->user_role = $data['user_role'];

        if (!$this->updateItem($teamMember, $data)) {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Data update is failed', ['data' => $data]);
        }

        $response = [
            'success' => true,
            'message' => 'Item was updated'
        ];

        return $this->createArrayResponse($response, 'data');
    }

    public function team($projectId)
    {
        $transformer = new ProjectTransformer;
        $transformer->setModelClass(Project::class);
        $project = $this->createItemResponse(Project::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $projectId,
            ]
        ]), $transformer);

        $response = [
            'team' => $project['team']
        ];

        return $response;
    }

    public function membersForAddToTeam($projectId)
    {
        $search = null;

        foreach ($this->query->getConditions() as $condition) {
            if ($condition->field === 'search') {
                $search = $condition->value;
            }
        }

        $transformer = new ProjectTransformer;
        $transformer->setModelClass(Project::class);
        $project = $this->createItemResponse(Project::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $projectId,
            ]
        ]), $transformer);
        $userQueryBind = [1 => User::STATE_ACTIVE];

        if ($search) {
            $userQueryBind[2] = '%' . $search . '%';
        }

        $members = User::find([
            'conditions' => 'state = ?1' . ($search ? ' AND (name LIKE :2: OR email LIKE :2:)' : ''),
            'bind' => $userQueryBind
        ]);
        $teamMemberIds = [];

        foreach ($project['team'] as $id => $teamMember) {
            $teamMemberIds[] = $teamMember['user_id'];
        }

        $membersToAddArray = [];

        foreach ($members as $id => $memberItem) {
            if (!in_array($memberItem->id, $teamMemberIds)) {
                $membersToAddArray[] = $members[$id];
            }
        }

        $result = [];
        $userTransformer = new UserTransformer;
        $userTransformer->setModelClass(User::class);

        foreach ($membersToAddArray as $memberItem) {
            $result[] = $this->createItemResponse($memberItem, $userTransformer);
        }

        $response = [
            'users' => $result
        ];

        return $response;
    }

    public function settingsFetch($projectId)
    {
        $userAccess = Project::checkUserAccess($projectId, $this->getUser()->id);

        if (!$userAccess || $userAccess->user_role != 1) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, 'Does not have access.');
        }

        $settings = ProjectSettings::find([
            'project_id = :project_id:',
            'bind' => [
                'project_id' => $projectId
            ]
        ]);

        return [
            'settings' => $settings
        ];
    }

    public function settingsSave($projectId)
    {
        $userAccess = Project::checkUserAccess($projectId, $this->getUser()->id);

        if (!$userAccess || $userAccess->user_role != 1) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, 'Does not have access.');
        }

        $settings = $this->request->getPostedData(PostedDataMethods::JSON_BODY);

        if (isset($settings['project_id'])) {
            unset($settings['project_id']);
        }

        if (isset($settings['creator_id'])) {
            unset($settings['creator_id']);
        }

        foreach ($settings as $name => $value) {
            $setting = ProjectSettings::findFirst([
                'project_id = :project_id: AND name = :name:',
                'bind' => [
                    'project_id' => $projectId,
                    'name' => $name
                ]
            ]);

            if ($setting) {
                $setting->value = $value;
            } else {
                $setting = new ProjectSettings([
                    'project_id' => $projectId,
                    'name' => $name,
                    'value' => $value
                ]);
            }

            if (!$setting->save()) {
                throw new Exception(ErrorCodes::DB_SAVE_ERROR, 'Save error.');
            }
        }

        return ['saved'];
    }

    public function whitelist()
    {
        return [
            'title',
            'description',
            'creator_id',
            'state'
        ];
    }

    public function export($id)
    {
        $token = $this->request->getHeader('X-TM-TOKEN');

        if ($token) {
            $apiKey = ApiKey::findFirst([
                'value = :value:',
                'bind' => [
                    'value' => $token
                ]
            ]);

            if ($apiKey && $apiKey->state == ApiKey::STATE_ACTIVE) {
                $project = Project::findFirst($id);

                if ($project && $project->state !== Project::STATE_ACTIVE) {
                    $transformer = new ProjectTransformer();
                    $transformer->setModelClass(Project::class);
                    $projectData = $this->createItemResponse($project, $transformer);

                    if (isset($projectData['creator'])) {
                        unset($projectData['creator']);
                    }

                    if (isset($projectData['creator_id'])) {
                        unset($projectData['creator_id']);
                    }

                    if (isset($projectData['team'])) {
                        unset($projectData['team']);
                    }

                    return ['data' => $projectData];
                } else {
                    return ['error' => 'Project fetching error.'];
                }
            } else {
                return ['error' => 'X-TM-TOKEN is not correct.'];
            }
        } else {
            return ['error' => 'X-TM-TOKEN is should be set.'];
        }
    }

    public function exportAll()
    {
        $token = $this->request->getHeader('X-TM-TOKEN');

        if ($token) {
            $apiKey = ApiKey::findFirst([
                'value = :value:',
                'bind' => [
                    'value' => $token
                ]
            ]);

            if ($apiKey && $apiKey->state == 1) {
                $projects = Project::find([
                    'conditions' => 'state = :state:',
                    'bind' => [
                        'state' => Project::STATE_ACTIVE
                    ]
                ]);

                if (!$projects) {
                    return ['error' => 'Projects fetching error.'];
                }

                $projectsData = [];

                foreach ($projects as $project) {
                    $transformer = new ProjectTransformer();
                    $transformer->setModelClass(Project::class);
                    $projectData = $this->createItemResponse($project, $transformer);

                    if (isset($projectData['creator'])) {
                        unset($projectData['creator']);
                    }

                    if (isset($projectData['creator_id'])) {
                        unset($projectData['creator_id']);
                    }

                    if (isset($projectData['team'])) {
                        unset($projectData['team']);
                    }

                    $projectsData[] = $projectData;
                }

                return ['data' => $projectsData];
            } else {
                return ['error' => 'X-TM-TOKEN is not correct.'];
            }
        } else {
            return ['error' => 'X-TM-TOKEN is should be set.'];
        }
    }
}
