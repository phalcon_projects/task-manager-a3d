<?php

namespace App\V1\Controllers;

use APP\REST\Mvc\Controllers\CrudResourceController;

class TimelineController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'id',
            'object_id',
            'object_type',
            'start_date'
        ];
    }
}
