<?php

namespace App\V1\Controllers;

use APP\API\Constants\ErrorCodes;
use APP\API\Data\Query;
use APP\API\Data\Query\Condition;
use APP\API\Exception;
use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\Model\Activity;
use App\V1\Model\Attachment;
use App\V1\Model\Project;
use App\V1\Model\Task;
use App\V1\Model\Timeline;
use App\V1\Model\TimelineSection;
use App\V1\Model\User;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Query\Builder as QueryBuilder;
use ZipArchive;
use TCPDF;

class TaskController extends CrudResourceController
{
    private $taskData;
    protected $isCreation = false;

    public function whitelist()
    {
        return [
            'title',
            'description',
            'project_id',
            'creator_id',
            'assignee_id',
            'primary_id',
            'state',
            'sort',
            'priority',
            'type',
            'due_date'
        ];
    }

    protected function afterHandleAll($data, &$response)
    {
        parent::afterHandleAll($data, $response);

        foreach ($response['tasks'] as $id => $item) {
            if (!Project::checkUserAccess($item['project_id'], $this->getUser()->id)) {
                unset($response['tasks'][$id]);
            }
        }
    }

    protected function afterHandleFind($item, $response)
    {
        if (!Project::checkUserAccess($response['task']['project_id'], $this->getUser()->id)) {
            throw new Exception(ErrorCodes::ACCESS_DENIED, 'Does not have access.');
        }
    }

    protected function beforeHandleAll()
    {
        $tasksByUsers = [
            'creator' => [],
            'assignee' => []
        ];

        $newConditions = [];

        foreach ($this->query->getConditions() as $condition) {
            if ($condition->operator == Query::OPERATOR_IS_NOT_EQUAL && $condition->field == 'id') {
                $tasks = Task::find([[
                    'conditions' => 'state != ?1',
                    'bind' => [
                        1 => Task::STATE_DELETED
                    ]
                ]]);
                $currentTaskId = '1';
                $excludeIds = [$currentTaskId];
                $this->checkSubTaskForExclude($currentTaskId, $excludeIds, $tasks);

                if (count($excludeIds) > 1) {
                    foreach ($excludeIds as $excludeId) {
                        $newConditions[] = new Condition(
                            Condition::TYPE_AND,
                            $condition->field,
                            Query::OPERATOR_IS_NOT_EQUAL,
                            $excludeId
                        );
                    }
                }
            }

            if ($condition->field == 'assignee' || $condition->field == 'creator') {
                if ($condition->operator == Query::OPERATOR_IS_LIKE) {
                    $users = User::find([
                        'conditions' => 'name like ?1',
                        'bind' => [
                            1 => '%' . $condition->value . '%'
                        ]
                    ]);
                    foreach ($users as $user) {
                        $newConditions[] = new Condition(
                            Condition::TYPE_AND,
                            $condition->field . '_id',
                            Query::OPERATOR_IS_EQUAL,
                            $user->id
                        );
                        $tasksByUsers[$condition->field][] = $user->id;
                    }
                }
                $this->query->removeConditionForField($condition->field);
            }

            if ($condition->field == 'title') {
                if ($condition->operator == Query::OPERATOR_IS_LIKE) {
                    $tasks = Task::find([
                        'conditions' => 'title like ?1',
                        'bind' => [
                            1 => '%' . $condition->value . '%'
                        ]
                    ]);
                    foreach ($tasks as $task) {
                        $newConditions[] = new Condition(
                            Condition::TYPE_OR,
                            'id',
                            Query::OPERATOR_IS_EQUAL,
                            $task->id
                        );
                    }
                }
                $this->query->removeConditionForField($condition->field);
            }
        }

        $idCondition = null;

        foreach ($this->query->getConditions() as $condition) {
            if ($condition->field == 'id') {
                $idCondition = $condition;
            }
        }

        $this->query->removeConditionForField('id');

        foreach ($newConditions as $condition) {
            if ($idCondition && $condition->field == 'id') {
                continue;
            }

            $this->query->addCondition($condition);
        }

        if ($idCondition) {
            $this->query->addCondition($idCondition);
        }

        return parent::beforeHandleAll();
    }

    protected function checkSubTaskForExclude($taskId, &$excludeIds, $tasks)
    {
        foreach ($tasks as $taskItem) {
            if ($taskItem->primary_id == $taskId && !in_array($taskItem->id, $excludeIds)) {
                $excludeIds[] = $taskItem->id;
                $this->checkSubTaskForExclude($taskItem->id, $excludeIds, $tasks);
            }
        }
    }

    public function getPostedData()
    {
        $data = parent::getPostedData();
        $user = $this->getUser();

        if ($user && !isset($data['creator_id'])) {
            $data['creator_id'] = $user->id;
        }

        if ($this->isCreation) {
            $queryParameters = ['order' => 'sort DESC'];

            if (isset($data['primary_id'])) {
                $queryParameters['conditions'] = 'primary_id = :primary_id:';
                $queryParameters['bind'] = [
                    'primary_id' => $data['primary_id']
                ];
            }

            $lastSubTask = Task::findFirst($queryParameters);

            if ($lastSubTask) {
                $data['sort'] = ++$lastSubTask->sort;
            }
        }

        return $data;
    }

    public function create()
    {
        $this->isCreation = true;
        return parent::create();
    }

    public function beforeHandleUpdate($id)
    {
        $this->taskData = Task::findFirst([
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $id,
            ]
        ]);
    }

    public function afterHandleUpdate(Model $updatedItem, $data, $response)
    {
        $this->updateActivity($updatedItem, $data, $this->taskData);

        if (isset($data['state']) && $data['state'] == Task::STATE_COMPLETED) {
            $tasks = Task::find([
                'conditions' => 'primary_id = ?1',
                'bind' => [
                    1 => $updatedItem->id,
                ]
            ]);

            foreach ($tasks as $task) {
                if ($task->state != Task::STATE_COMPLETED) {
                    $task->state = Task::STATE_COMPLETED;

                    if (!$task->save()) {
                        return new Exception(
                            ErrorCodes::DATA_FAILED,
                            'Cannot update sub task state',
                            ['data' => $task]
                        );
                    }

                    $this->updateActivity($task, $data);
                }
            }
        }

        if ($updatedItem->state === Task::STATE_COMPLETED) {
            $this->updateTimelineToComplete($updatedItem);
        }
    }

    protected function updateTimelineToComplete($task)
    {
        if ($task->primary_id == 0) {
            $taskTimelines = Timeline::find([
                'conditions' => 'object_id = :taskId: AND object_type = "task"',
                'bind' => [
                    'taskId' => $task->id
                ]
            ]);

            if (!empty($taskTimelines)) {
                $timeline = $taskTimelines[0];
                $timelineSections = TimelineSection::find([
                    'conditions' => 'timeline_id = :timelineId:',
                    'bind' => [
                        'timelineId' => $timeline->id
                    ]
                ]);
                foreach ($timelineSections as $timelineSection) {
                    $timelineSection->state = TimelineSection::STATE_COMPLETED;
                    try {
                        if (!$timelineSection->save()) {
                            throw new Exception(ErrorCodes::DB_SAVE_ERROR, 'Error save timeline state.', $task);
                        }
                    } catch (Exception $e) {
                        return $e;
                    }
                }
            }
        }
    }

    protected function updateActivity($task, $newData, $previousData = null)
    {
        $activityDescription = 'the task was updated';

        /*if ($previousData !== null) {
            /* set activity description by changed task parameter - changed one by request */

        /*if ($newData['title'] !== $previousData['title']) {
            $activityDescription = 'Title changed to ' . $previousData['title'];
        }
        if ($newData['description'] !== $previousData['description']) {
            $activityDescription = ' changed to ' . $previousData['description'];
        }
    }*/

        $activityData = [
            'object_id' => $task->id,
            'object_type' => 'task',
            'creator_id' => $newData['creator_id'],
            'project_id' => $task->project_id,
            'type' => 'task',
            'content' => $activityDescription
        ];
        $activity = new Activity($activityData);

        if (!$activity->save()) {
            $this->onCreateFailed($activity, $activityData);
        }

        return true;
    }

    public function getImages($taskId)
    {
        if (!$taskId) {
            return new Exception(ErrorCodes::DATA_FAILED, 'Task id is not set.');
        }

        $result = [];
        $subTasksIds = [];
        $subTasks = Task::find([
            'columns' => 'id',
            'conditions' => 'primary_id = :task_id: AND (state = :state_active: OR state = :state_completed:)',
            'bind' => [
                'task_id' => $taskId,
                'state_active' => Task::STATE_ACTIVE,
                'state_completed' => Task::STATE_COMPLETED
            ]
        ]);

        foreach ($subTasks as $subTask) {
            $subTasksIds[] = $subTask['id'];
        }

        if ($subTasksIds) {
            $attachments = Attachment::query()
                ->inWhere('object_id', $subTasksIds)
                ->andWhere('object_type = "task"')
                ->execute();

            if ($attachments && count($attachments) > 0) {
                $zip = new ZipArchive();
                $zipName = 'files/task_' . $taskId . '_images.zip';

                if (file_exists($zipName)) {
                    unlink($zipName);
                }

                $zip->open($zipName, ZipArchive::CREATE);

                $newWidth = false;

                if ($this->request->get('is_original') == 'false') {
                    if ($this->request->get('width')) {
                        $newWidth = $this->request->get('width');
                    } else {
                        throw new Exception(
                            ErrorCodes::DATA_FAILED,
                            'The new image width is need to be set.'
                        );
                    }
                }

                foreach ($attachments as $attachment) {
                    $filePath = PUBLIC_PATH . '/' . $attachment->path;

                    if (file_exists($filePath)) {
                        if ($newWidth) {
                            $path = explode('uploads/', $attachment->path);
                            $fileName = $path[1];
                            $newImagePath = $this->imageResize(
                                $filePath,
                                $newWidth,
                                null,
                                false,
                                $fileName
                            );
                            $filePath = $newImagePath ? PUBLIC_PATH . '/' . $newImagePath : null;
                        }

                        if ($filePath) {
                            $zip->addFromString(basename($filePath), file_get_contents($filePath));
                        } else {
                            $response['error'] = 'Error adding some files.';
                        }
                    } else {
                        $result['error'] = 'Error adding some files.';
                    }
                }

                $zip->close();
                $result['images'] = $zipName;
            } else {
                $result['images'] = null;
            }
        } else {
            $result['images'] = null;
        }

        return $result;
    }

    public function imageResize($path, $width, $height = null, $crop = false, $resultFileName = null)
    {
        list($originalWidth, $originalHeight) = getimagesize($path);
        $ratio = $originalWidth / $originalHeight;

        if ($crop) {
            if ($originalWidth > $originalHeight) {
                $originalWidth = ceil($originalWidth - ($originalWidth * abs($ratio - $width / $height)));
            } else {
                $originalHeight = ceil($originalHeight - ($originalHeight * abs($ratio - $width / $height)));
            }

            $newWidth = $width;
            $newHeight = $height;
        } else {
            if ($height && $width / $height > $ratio) {
                $newWidth = $height * $ratio;
                $newHeight = $height;
            } else {
                $newHeight = $width / $ratio;
                $newWidth = $width;
            }
        }

        $source = imagecreatefromstring(file_get_contents($path));
        $result = imagecreatetruecolor($newWidth, $newHeight);
        $newFilePath = null;

        if (imagecopyresampled(
            $result,
            $source,
            0,
            0,
            0,
            0,
            $newWidth,
            $newHeight,
            $originalWidth,
            $originalHeight
        )) {
            $newFilePath = 'files/temp/' . $resultFileName;
            imagejpeg($result, PUBLIC_PATH . '/' . $newFilePath, 100);
        }

        return $newFilePath;
    }

    public function getPDFImage($subTaskId)
    {
        $result = null;

        $image = Attachment::findFirst([
            'conditions' => 'object_id = :object_id: AND object_type = "task"',
            'bind' => [
                'object_id' => $subTaskId
            ]
        ]);

        $pdf = new TCPDF();
        $pdf->AddPage();
        $pdf->Image(PUBLIC_PATH . '/' . $image->path, 5, 5, 200);
        $pdfResultFile = 'files/sub-task-' . $subTaskId . '.pdf';
        $pdfResult = $pdf->Output(PUBLIC_PATH . '/' . $pdfResultFile, 'F');

        if ($pdfResult) {
            $result = 'Failed to export image.';
        } else {
            $result = $pdfResultFile;
        }

        return ['image' => $result];
    }

    public function sortReplace()
    {
        $data = $this->getPostedData();

        if (!isset($data['target']) || !isset($data['replace'])) {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Error sorting tasks ids.');
        }

        $target = Task::findFirst($data['target']);
        $replace = Task::findFirst($data['replace']);

        if (!$target || !$replace) {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Error tasks fetching.');
        }

        $targetSort = $target->sort;
        $target->sort = $replace->sort;
        $replace->sort = $targetSort;

        if (!$target->save() || !$replace->save()) {
            throw new Exception(ErrorCodes::DB_SAVE_ERROR, 'Error saving new data.');
        }

        return ['sorted' => true];
    }
}
