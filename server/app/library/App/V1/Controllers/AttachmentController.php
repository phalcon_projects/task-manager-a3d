<?php

namespace App\V1\Controllers;

use APP\API\Constants\ErrorCodes;
use APP\API\Exception;
use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\Model\Activity;
use App\V1\Model\Attachment;
use App\V1\Transformers\AttachmentTransformer;
use Phalcon\Mvc\Model as Model;

class AttachmentController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'title',
            'description',
            'path',
            'type',
            'creator_id',
            'object_id',
            'object_type'
        ];
    }

    public function getPostedData()
    {
        $data = parent::getPostedData();
        $user = $this->getUser();

        if ($user && !isset($data['creator_id'])) {
            $data['creator_id'] = $user->id;
        }

        if (isset($data['file']) && isset($data['file']['path'])) {
            $data['path'] = $data['file']['path'];
            $data['type'] = $data['file']['extension'];
            $data['title'] = $data['file']['name'];
            unset($data['file']);
        }

        return $data;
    }

    public function replace()
    {
        try {
            $data = $this->getPostedData();

            if (
                !$this->request->isPost()
                || !isset($data['id'])
                || !isset($data['path'])
                || !isset($data['title'])
                || !isset($data['type'])
            ) {
                $this->onDataInvalid($data);
            }

            $attachment = Attachment::findFirst($data['id']);
            $currentFilePath = PUBLIC_PATH . '/' . str_replace('\\', '/', $attachment->path);

            if (file_exists(PUBLIC_PATH . '/' . $attachment->path)) {
                unset($currentFilePath);
            }

            if (!$attachment) {
                if (file_exists(PUBLIC_PATH . '/' . $data['path'])) {
                    unset($currentFilePath);
                }

                $this->onDataInvalid($attachment);
            }

            $attachment->title = $data['title'];
            $attachment->path = $data['path'];
            $attachment->type = $data['type'];

            if ($attachment->save()) {
                $result = (new AttachmentTransformer())->transform($attachment);

                return [
                    'success' => true,
                    'attachment' => $result
                ];
            } else {
                $errors = [];
                foreach ($attachment->getMessages() as $error) {
                    $errors[] = $error;
                }
                throw new Exception(
                    ErrorCodes::DATA_FAILED,
                    'Failed to save item.',
                    ['errors' => $errors]
                );
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    /*protected function afterHandleCreate(Model $createdItem, $data, $response)
    {
        try {
            if (in_array($createdItem->object_type, ['task', 'discussion'])) {
                $activityData = [
                    'object_id' => $data['object_id'],
                    'object_type' => $data['object_type'],
                    'related_id' => $createdItem->id,
                    'creator_id' => $data['creator_id'],
                    'project_id' => $data['project_id'],
                    'type' => 'attachment',
                    'content' => 'attached a file'
                ];
                $activity = new Activity($activityData);

                if (!$activity->save()) {
                    $this->onCreateFailed($activity, $activityData);
                }
            }
        } catch (Exception $e) {
            return $e;
        }
    }*/

    protected function afterHandleRemove(Model $removedItem, $response)
    {
        try {
            $filePath = PUBLIC_PATH . '/' . $removedItem->path;

            if (!$removedItem->delete()) {
                throw new Exception(ErrorCodes::DATA_FAILED, 'Error attachment remove.');
            } else {
                if (!file_exists($filePath)) {
                    throw new Exception(
                        ErrorCodes::DATA_FAILED,
                        'Attachment file does not exist.'
                    );
                } elseif (!unlink($filePath)) {
                    throw new Exception(
                        ErrorCodes::DATA_FAILED,
                        'Error attachment file removing.'
                    );
                }
            }
        } catch (Exception $e) {
            return $e;
        }
    }
}
