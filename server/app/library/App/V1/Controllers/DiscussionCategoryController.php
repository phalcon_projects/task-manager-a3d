<?php

namespace App\V1\Controllers;

use APP\REST\Mvc\Controllers\CrudResourceController;

class DiscussionCategoryController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'object_id',
            'creator_id',
            'title',
            'description',
            'updated_at'
        ];
    }
}
