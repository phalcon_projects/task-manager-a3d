<?php

namespace App\V1\Controllers;

use APP\API\Constants\ErrorCodes;
use APP\API\Exception;
use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\Model\Attachment;
use Phalcon\Mvc\Model as Model;

class ActivityController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'object_id',
            'object_type',
            'creator_id',
            'related_id',
            'related_user_id',
            'project_id',
            'type',
            'content',
            'state',
            'created_at',
            'updated_at'
        ];
    }

    public function getPostedData()
    {
        $data = parent::getPostedData();
        $user = $this->getUser();

        if ($user && !isset($data['creator_id'])) {
            $data['creator_id'] = $user->id;
        }

        return $data;
    }

    protected function afterHandleRemove(Model $removedItem, $response)
    {
        $id = $removedItem->id;

        if (!$removedItem->delete()) {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Error activity remove.');
        } else {
            $attachments = Attachment::find([
                'object_type = "comment" AND object_id = :1:',
                'bind' => [1 => $id]
            ]);

            foreach ($attachments as $attachment) {
                if (!$attachment->delete()) {
                    throw new Exception(ErrorCodes::DATA_FAILED, 'Error activity attachment remove.');
                }
            }
        }
    }
}
