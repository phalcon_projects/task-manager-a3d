<?php

namespace App\V1\Controllers;

use APP\API\Constants\ErrorCodes;
use APP\API\Exception;
use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\Auth\EmailAccountType;
use App\V1\Constants\Services;
use App\V1\Model\User;
use App\V1\Model\ProjectUserRole;
use App\V1\Transformers\UserTransformer;
use App\V1\User\Service;
use App\V1\User\States;
use Phalcon\Di;

class UserController extends CrudResourceController
{
    protected $isCreation = false;

    public function me()
    {
        return $this->createResourceResponse($this->userService->getDetails());
    }

    public function authenticate()
    {
        $login = $this->request->getUsername();
        $password = $this->request->getPassword();

        $session = $this->authManager->loginWithEmailPassword(EmailAccountType::NAME, $login, $password);

        $transformer = new UserTransformer;
        $transformer->setModelClass(User::class);

        $userData = User::findFirst([
            'columns' => 'id, login, email, name, password, role, state, created_at, updated_at',
            'conditions' => 'id = ?1 AND state = ?2 OR state = ?3',
            'bind' => [
                1 => $session->getIdentity(),
                2 => User::STATE_ACTIVE,
                3 => User::STATE_PENDING
            ]
        ]);
        $user = new User($userData->toArray());

        if ($userData['state'] == States::PENDING) {
            if (!$user->update(['state' => States::ACTIVE])) {
                $messages = $user->getMessages();

                throw new Exception(ErrorCodes::GENERAL_SYSTEM, implode(',', $messages));
            }
        }

        unset($user->password);
        $userResponse = $this->createItemResponse($user, $transformer);

        $response = [
            'token' => $session->getToken(),
            'expire' => $session->getExpirationTime(),
            'user' => $userResponse
        ];

        return $this->createArrayResponse($response, 'data');
    }

    public function beforeHandleCreate()
    {
        $data = $this->getPostedData();

        if (!isset($data['email']) || !isset($data['login']) || !isset($data['password'])) {
            throw new Exception(
                ErrorCodes::DATA_FAILED,
                'Not all required fields are specifyed.',
                ['data' => $data]
            );
        }

        $checkEmailUser = User::findFirst([
            'conditions' => 'email = :email:',
            'bind' => [
                'email' => $data['email']
            ]
        ]);

        if ($checkEmailUser) {
            throw new Exception(
                ErrorCodes::MEMBER_EXISTS,
                'The account with specified email already registered.',
                ['data' => $data]
            );
        }

        $checkLoginUser = User::findFirst([
            'conditions' => 'login = :login:',
            'bind' => [
                'login' => $data['login']
            ]
        ]);

        if ($checkLoginUser) {
            throw new Exception(
                ErrorCodes::MEMBER_EXISTS,
                'The account with specified login already registered.',
                ['data' => $data]
            );
        }
    }

    protected function getPostedData()
    {
        $data = parent::getPostedData();

        if (isset($data['password'])) {
            $security = Di::getDefault()->get(Services::SECURITY);
            $data['password'] = $security->hash($data['password']);
        }

        return $data;
    }

    public function update($id)
    {
        $id = (int)$id;

        $data = $this->request->getPostedData();
        $result = parent::update($id);

        if (isset($data['password'])) {
            $user = User::findFirst([
                'conditions' => 'id = ?1',
                'bind' => [
                    1 => $id
                ]
            ]);

            if (!$user) {
                return $this->onItemNotFound($id);
            }

            $security = Di::getDefault()->get(Services::SECURITY);
            $user->password = $security->hash($data['password']);
            $user->save();
        }

        return $result;
    }

    public function invite()
    {
        $data = $this->request->getPostedData();

        $transformer = new UserTransformer;
        $transformer->setModelClass(User::class);

        $requiredFields = ['email', 'role_id'];
        $notExistsValues = array_diff_key($requiredFields, array_keys($data));

        if (!empty($notExistsValues)) {
            return $this->onDataInvalid($data);
        }

        if ($user = User::findFirst([
            'columns' => 'email',
            'conditions' => 'email = ?1',
            'bind' => [
                1 => $data['email']
            ]
        ])) {
            $this->onMemberInviteFailed($data);
        }

        if (!ProjectUserRole::findFirst([
            'columns' => 'id',
            'conditions' => 'id = ?1',
            'bind' => [
                1 => $data['role_id'],
            ]
        ])) {
            return $this->onDataInvalid($data);
        }

        $password = Service::generateSecureString(10, 'lower_case,upper_case,numbers,special_symbols');
        $emailSentResult = $this->userService->sendInvitationEmail(array_merge($data, ['password' => $password]));

        if (!isset($emailSentResult['success']) || !$emailSentResult['success']) {
            $this->onMemberInvitationEmailSendFailed($data);
        }

        $user = new User();
        $user->login = $data['email'];
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->role = $data['role_id'];
        $user->state = States::PENDING;
        $user->password = $this->getDI()->getSecurity()->hash($password);

        if (!$createResult = $user->create()) {
            return $this->onCreateFailed($user, $data);
        }

        $userResponse = $this->createItemResponse(User::findFirst([
            'columns' => 'id, login, email, name, role, state, created_at, updated_at',
            'conditions' => 'email = ?1',
            'bind' => [
                1 => $data['email'],
            ]
        ]), $transformer);

        $response = [
            'user' => $userResponse
        ];

        return $this->createArrayResponse($response, 'data');
    }

    protected function onMemberInviteFailed($data)
    {
        throw new Exception(ErrorCodes::MEMBER_EXISTS, null, ['data' => $data]);
    }

    protected function onMemberInvitationEmailSendFailed($data)
    {
        throw new Exception(ErrorCodes::MEMBER_INVITATION_EMAIL_SEND_FAILED, null, ['data' => $data]);
    }

    public function whitelist()
    {
        return [
            'login',
            'name',
            'email',
            'password',
            'role',
            'state'
        ];
    }
}
