<?php

namespace App\V1\Controllers;

use APP\REST\Mvc\Controllers\CrudResourceController;

class NotificationController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'user_id',
            'object_id',
            'object_type',
            'object_link',
            'related_user_id',
            'data',
            'state'
        ];
    }

    protected function afterHandleAll($data, &$response)
    {
        foreach ($response['notifications'] as $id => $notification) {
            if ($notification['user_id'] == $notification['related_user_id']) {
                unset($response['notifications'][$id]);
            }
        }
    }
}
