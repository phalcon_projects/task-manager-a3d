<?php

namespace App\V1\Controllers;

use APP\REST\Mvc\Controllers\CrudResourceController;

class UserRoleController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'title',
            'alias'
        ];
    }
}
