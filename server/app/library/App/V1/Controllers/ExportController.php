<?php

namespace App\V1\Controllers;

use App\V1\Constants\Services;
use APP\REST\Export\Documentation;
use APP\REST\Export\Postman\ApiCollection;
use APP\REST\Mvc\Controllers\CollectionController;
use APP\REST\Transformers\DocumentationTransformer;
use APP\REST\Transformers\Postman\ApiCollectionTransformer;

class ExportController extends CollectionController
{
    public function documentation()
    {
        /** @var \Phalcon\Config $config */
        $config = $this->di->get(Services::CONFIG);

        $documentation = new Documentation($config->application->title, $config->hostName);
        $documentation->addManyCollections($this->application->getCollections());
        $documentation->addManyRoutes($this->application->getRouter()->getRoutes());

        return $this->createItemResponse($documentation, new DocumentationTransformer(), 'documentation');
    }

    public function postman()
    {
        /** @var \Phalcon\Config $config */
        $config = $this->di->get(Services::CONFIG);

        $postmanCollection = new ApiCollection($config->application->title, $config->hostName);
        $postmanCollection->addManyCollections($this->application->getCollections());
        $postmanCollection->addManyRoutes($this->application->getRouter()->getRoutes());

        return $this->createItemResponse($postmanCollection, new ApiCollectionTransformer());
    }
}
