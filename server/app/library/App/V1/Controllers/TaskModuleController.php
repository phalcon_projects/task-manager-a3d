<?php

namespace App\V1\Controllers;

use APP\API\Constants\ErrorCodes;
use APP\API\Exception;
use APP\REST\Mvc\Controllers\CrudResourceController;
use App\V1\Model\Activity;
use App\V1\Model\Attachment;
use App\V1\Model\Task;
use Phalcon\Mvc\Model as Model;

class TaskModuleController extends CrudResourceController
{
    public function whitelist()
    {
        return [
            'creator_id',
            'task_id',
            'type',
            'source_data',
            'source_type'
        ];
    }

    public function getPostedData()
    {
        $data = parent::getPostedData();
        $user = $this->getUser();

        if ($user && !isset($data['creator_id'])) {
            $data['creator_id'] = $user->id;
        }

        return $data;
    }

    protected function afterHandleRemove(Model $removedItem, $response)
    {
        $id = $removedItem->id;

        if (!$removedItem->delete()) {
            throw new Exception(ErrorCodes::DATA_FAILED, 'Error module remove.');
        } else {
            $attachments = Attachment::find([
                'object_type = "module" AND object_id = :1:',
                'bind' => [1 => $id]
            ]);

            foreach ($attachments as $attachment) {
                if (!$attachment->delete()) {
                    throw new Exception(ErrorCodes::DATA_FAILED, 'Error module attachment remove.');
                }
            }
        }
    }

    protected function afterHandleUpdate(Model $updatedItem, $data, $response)
    {
        $task = Task::findFirst($updatedItem->task_id);

        if ($task) {
            $activityDescription = 'the task modules was updated';

            $activityData = [
                'object_id' => $task->id,
                'object_type' => 'task',
                'creator_id' => $updatedItem->creator_id,
                'project_id' => $task->project_id,
                'type' => 'module',
                'content' => $activityDescription
            ];
            $activity = new Activity($activityData);

            if (!$activity->save()) {
                $this->onCreateFailed($activity, $activityData);
            }
        }

        return true;
    }
}
