<?php

namespace APP\API\Mvc;

/**
 * APP\API\Mvc\Plugin
 * This class allows to access services in the services container by just only accessing a public property
 * with the same name of a registered service
 *
 * @property \APP\API\Api $application
 * @property \APP\API\Http\Request $request
 * @property \APP\API\Http\Response $response
 * @property \Phalcon\Acl\AdapterInterface $acl
 * @property \APP\API\Auth\Manager $authManager
 * @property \APP\API\User\Service $userService
 * @property \APP\API\Helpers\ErrorHelper $errorHelper
 * @property \APP\API\Helpers\FormatHelper $formatHelper
 * @property \APP\API\Auth\TokenParserInterface $tokenParser
 * @property \APP\API\Data\Query $query
 * @property \APP\API\Data\Query\QueryParsers\UrlQueryParser $urlQueryParser
 */

class Plugin extends \Phalcon\Mvc\User\Plugin
{

}