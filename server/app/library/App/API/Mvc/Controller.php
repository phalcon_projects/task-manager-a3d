<?php

namespace APP\API\Mvc;

/**
 * @property \APP\API\Api $application
 * @property \APP\API\Http\Request $request
 * @property \APP\API\Http\Response $response
 * @property \Phalcon\Acl\AdapterInterface $acl
 * @property \APP\API\Auth\Manager $authManager
 * @property \APP\API\User\Service $userService
 * @property \APP\API\Helpers\ErrorHelper $errorHelper
 * @property \APP\API\Helpers\FormatHelper $formatHelper
 * @property \APP\API\Auth\TokenParserInterface $tokenParser
 * @property \APP\API\Data\Query $query
 * @property \APP\API\Data\Query\QueryParsers\UrlQueryParser $urlQueryParser
 */

class Controller extends \Phalcon\Mvc\Controller
{

}