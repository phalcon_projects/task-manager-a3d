<?php

namespace APP\API;

use APP\API\Constants\Services;
use Phalcon\Mvc\Micro;

/**
 * Class APP\Api
 * @package APP\API
 *
 * @property Api $application
 * @property \APP\API\Http\Request $request
 * @property \APP\API\Http\Response $response
 * @property \APP\API\Auth\Manager $authManager
 * @property \APP\API\User\Service $userService
 * @property \APP\API\Auth\TokenParserInterface $tokenParser
 * @property \APP\API\Data\Query $query
 * @property \APP\API\Data\Query\QueryParsers\UrlQueryParser $urlQueryParser
 * @property \Phalcon\Acl\AdapterInterface $acl
 */
class Api extends Micro
{
    /**
     * Attaches middleware to the APP\API
     *
     * @param $middleware
     *
     * @return static
     */
    public function attach($middleware)
    {
        if (!$this->getEventsManager()) {
            $this->setEventsManager($this->getDI()->get(Services::EVENTS_MANAGER));
        }

        $this->getEventsManager()->attach('micro', $middleware);

        return $this;
    }
}
