<?php

namespace APP\API\Acl\Adapter;

use APP\API\Acl\MountingEnabledAdapterInterface;

class Memory extends \Phalcon\Acl\Adapter\Memory implements MountingEnabledAdapterInterface
{
    use \AclAdapterMountTrait;
}
