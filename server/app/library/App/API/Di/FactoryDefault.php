<?php

namespace APP\API\Di;

use APP\API\Acl\Adapter\Memory as Acl;
use APP\API\Auth\Manager as AuthManager;
use APP\API\Auth\TokenParsers\JWTTokenParser;
use APP\API\Constants\Services;
use APP\API\Data\Query;
use APP\API\Data\Query\QueryParsers\UrlQueryParser;
use APP\API\Helpers\ErrorHelper;
use APP\API\Helpers\FormatHelper;
use APP\API\Http\Request;
use APP\API\Http\Response;
use APP\API\User\Service as UserService;

class FactoryDefault extends \Phalcon\Di\FactoryDefault
{
    public function __construct()
    {
        parent::__construct();

        $this->setShared(Services::REQUEST, new Request);
        $this->setShared(Services::RESPONSE, new Response);

        $this->setShared(Services::AUTH_MANAGER, new AuthManager);

        $this->setShared(Services::USER_SERVICE, new UserService);

        $this->setShared(Services::TOKEN_PARSER, function () {

            return new JWTTokenParser('this_should_be_changed');
        });

        $this->setShared(Services::QUERY, new Query);

        $this->setShared(Services::URL_QUERY_PARSER, new UrlQueryParser);

        $this->setShared(Services::ACL, new Acl);

        $this->setShared(Services::ERROR_HELPER, new ErrorHelper);

        $this->setShared(Services::FORMAT_HELPER, new FormatHelper);
    }
}
