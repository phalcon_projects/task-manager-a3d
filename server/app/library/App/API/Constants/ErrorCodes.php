<?php

namespace APP\API\Constants;

class ErrorCodes
{
    // General
    const GENERAL_SYSTEM = 1010;
    const GENERAL_NOT_IMPLEMENTED = 1020;
    const GENERAL_NOT_FOUND = 1030;

    // Authentication
    const AUTH_INVALID_ACCOUNT_TYPE = 2010;
    const AUTH_LOGIN_FAILED = 2020;
    const AUTH_TOKEN_INVALID = 2030;
    const AUTH_SESSION_EXPIRED = 2040;
    const AUTH_SESSION_INVALID = 2050;

    // Access Control
    const ACCESS_DENIED = 3010;

    // Data
    const DATA_FAILED = 4010;
    const DATA_NOT_FOUND = 4020;

    const POST_DATA_NOT_PROVIDED = 5010;
    const POST_DATA_INVALID = 5020;

    // Mail
    const MAIL_SERVER_ERROR = 6010;

    // User
    const MEMBER_EXISTS = 7010;
    const MEMBER_INVITATION_EMAIL_SEND_FAILED = 7020;

    // Internal
    const DB_SAVE_ERROR = 8010;
}
