<?php

return [
    'debug' => true,
    'hostName' => 'SERVER_HOST_NAME',// example http://example.com
    'clientHostName' => 'CLIENT_HOST_NAME',// example http://example.com
    'database' => [
        // Change to your own configuration
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'dbname' => 'DB_NAME',
        'username' => 'DB_USER_NAME',
        'password' => 'DB_PASSWORD',
        'charset' => 'utf8',
    ],
    'cors' => [
        'allowedOrigins' => ['*'],
        'allowedMethods' => ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS', 'PATCH'],
        'allowedHeaders' => ['Content-Type', 'X-Requested-With', 'Authorization']
    ]
];
