<?php

return [
    'debug' => true,
    'hostName' => 'http://task-service.visionlab.de',
    'clientHostName' => 'http://task.visionlab.de',
    'database' => [
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'dbname' => 'task_manager_a3d',
        'username' => 'tma3d',
        'password' => 'cpSEhm~kmePQWQ59',
        'charset' => 'utf8',
    ],
    'cors' => [
        'allowedOrigins' => ['*'],
        'allowedMethods' => ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS', 'PATCH'],
        'allowedHeaders' => ['Content-Type', 'X-Requested-With', 'Authorization', 'X-TM-TOKEN']
    ],
    'mail' => [
        'from' => 'info@visionlab.de'
    ]
];
