<?php

return [
    'application' => [
        'title' => 'APP_TITLE',
        'description' => 'APP_DESCRIPTION',
        'baseUri' => '/',
        'viewsDir' => __DIR__ . '/../views/',
    ],

    'api' => [
        'version' => 'API_VERSION'
    ],

    'authentication' => [
        'secret' => 'APP_SECRET',
        'expirationTime' => 86400 * 7, // One week till token expires
    ]
];
