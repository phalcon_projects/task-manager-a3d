<?php

return [
    'debug' => true,
    'hostName' => 'http://task-manager-a3d.loc',
    'clientHostName' => 'http://localhost:8080',
    'database' => [
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'dbname' => 'task_manager_a3d',
        'username' => 'tma3d',
        'password' => '4cCdMWM7du6YRxgU',
        'charset' => 'utf8',
    ],
    'cors' => [
        'allowedOrigins' => ['*'],
        'allowedMethods' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
        'allowedHeaders' => ['Content-Type', 'X-Requested-With', 'Authorization', 'X-TM-TOKEN']
    ],
    'mail' => [
        'from' => 'dev@task-manager.com',
        'smtp' => [
            'host' => 'smtp.mailtrap.io',
            'port' => '465',//25 or 465 or 2525
            'username' => 'e83a7b9376a979',
            'password' => '3b50b5c347a793',
            'SMTPAuth' => true,
            'SMTPSecure' => 'tls'
        ]
    ]
];
