<?php

return [
    'debug' => true,
    'hostName' => 'http://service-task-manager-a3d.happyuser.info:9000',
    'clientHostName' => 'http://task-manager-a3d.happyuser.info',
    'database' => [
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'dbname' => 'task_manager_a3d',
        'username' => 'task_manager_a3d',
        'password' => 'gkA9sksH9SWv9VKK',
        'charset' => 'utf8',
    ],
    'cors' => [
        'allowedOrigins' => ['*'],
        'allowedMethods' => ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS', 'PATCH'],
        'allowedHeaders' => ['Content-Type', 'X-Requested-With', 'Authorization', 'X-TM-TOKEN']
    ],
    'mail' => [
        'from' => 'dev@task-manager.com',
        'smtp' => [
            'host' => 'smtp.mailtrap.io',
            'port' => '465',//25 or 465 or 2525
            'username' => 'e83a7b9376a979',
            'password' => '3b50b5c347a793',
            'SMTPAuth' => true,
            'SMTPSecure' => 'tls'
        ]
    ]
];
