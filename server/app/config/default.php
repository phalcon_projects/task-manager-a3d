<?php

return [
    'application' => [
        'title' => 'Task Manager',
        'description' => 'Task Manager integration tool for Augmented 3D.',
        'baseUri' => '/',
        'viewsDir' => __DIR__ . '/../views/',
    ],

    'api' => [
        'version' => 'v1'
    ],

    'authentication' => [
        'secret' => 'APP_SECRET',
        'expirationTime' => 86400 * 7, // One week till token expires
    ]
];
