<?php

use App\API\Http\Response;
use App\V1\Bootstrap;
use App\V1\Bootstrap\AclBootstrap;
use App\V1\Bootstrap\CollectionBootstrap;
use App\V1\Bootstrap\MiddlewareBootstrap;
use App\V1\Bootstrap\RouteBootstrap;
use App\V1\Bootstrap\ServiceBootstrap;
use App\V1\Constants\Services;
use App\REST\Api;
use App\REST\Di\FactoryDefault;
use Phalcon\Config;
use Phalcon\Loader;

/** @var Config $config */
$config = null;

/** @var Api $app */
$app = null;

/** @var Response $response */
$response = null;

try {
    define('ROOT_PATH', __DIR__ . '/..');
    define('PUBLIC_PATH', __DIR__);
    define('APP_PATH', ROOT_PATH . '/app');
    define('VENDOR_PATH', ROOT_PATH . '/vendor');
    define('CONFIG_PATH', APP_PATH . '/config');

    define('APPLICATION_ENV', getenv('APPLICATION_ENV') ?: 'development');

    // Autoload dependencies
    require VENDOR_PATH . '/autoload.php';

    $loader = new Loader();

    $loader->registerNamespaces([
        'App' => APP_PATH . '/library/App'
    ]);

    $loader->registerDirs([
        APP_PATH . '/views/'
    ]);

    $loader->register();

    // Config
    $configPath = CONFIG_PATH . '/default.php';

    if (!is_readable($configPath)) {
        throw new Exception('Unable to read config from ' . $configPath);
    }

    $config = new Config(include_once $configPath);
    $envConfigPath = CONFIG_PATH . '/server.' . APPLICATION_ENV . '.php';

    if (!is_readable($envConfigPath)) {
        throw new Exception('Unable to read config from ' . $envConfigPath);
    }

    $override = new Config(include_once $envConfigPath);
    $config = $config->merge($override);

    // Instantiate application & DI
    $di = new FactoryDefault();
    $app = new Api($di);

    // Bootstrap components
    $bootstrap = new Bootstrap(
        new ServiceBootstrap,
        new MiddlewareBootstrap,
        new CollectionBootstrap,
        new RouteBootstrap,
        new AclBootstrap
    );

    $bootstrap->run($app, $di, $config);

    // Start application
    $app->handle();

    // Set appropriate response value
    $response = $app->di->getShared(Services::RESPONSE);
    $returnedValue = $app->getReturnedValue();

    if ($returnedValue !== null) {

        if (is_string($returnedValue)) {
            $response->setContent($returnedValue);
        } else {
            $response->setJsonContent($returnedValue);
        }
    }

} catch (Exception $e) {
    // Handle exceptions
    $di = $app && $app->di ? $app->di : new FactoryDefault();
    $response = $di->getShared(Services::RESPONSE);
    if (!$response || !$response instanceof Response) {
        $response = new Response();
    }

    $debugMode = isset($config->debug) ? $config->debug : (APPLICATION_ENV == 'development');

    $response->setErrorContent($e, $debugMode);
} finally {

    // Send response
    if (!$response->isSent()) {
        $response->send();
    }
}
