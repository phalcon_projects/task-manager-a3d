<?php

return [
    'application' => [
        'title' => 'Phalcon REST Service',
        'description' => 'Phalcon REST Service.',
        'baseUri' => '/',
        'viewsDir' => __DIR__ . '/../views/',
    ],
    'authentication' => [
        'secret' => 'AUTH_SECRET',
        'expirationTime' => 86400 * 7, // One week till token expires
    ],
    'debug' => true,
    'hostName' => 'SERVER_HOST_NAME',
    'clientHostName' => 'CLIENT_HOST_NAME',
    'database' => [
        // Change to your own configuration
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'username' => 'DB_USER_NAME',
        'password' => 'DB_PASSWORD',
        'name' => 'DB_NAME',
    ],
    'cors' => [
        'allowedOrigins' => ['SERVER_HOST_NAME']
    ]
];
