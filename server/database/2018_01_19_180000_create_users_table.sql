DROP TABLE IF EXISTS users;

CREATE TABLE users
(
  id         INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
  login      VARCHAR(255)                                NOT NULL,
  email      VARCHAR(255)                                NOT NULL,
  password   VARCHAR(255)                                NOT NULL,
  name       VARCHAR(255)                                NOT NULL,
  role       TINYINT(4) UNSIGNED DEFAULT '2'             NOT NULL,
  state      TINYINT(4) DEFAULT '0'                      NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP         NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP         NOT NULL
)
  ENGINE = InnoDB;

INSERT INTO users (id, login, email, password, name, role, state, created_at, updated_at) VALUES
  (1, 'admin', 'admin@tm.com', '$2y$10$1/U1yo5yMdXsrsU3RaeULu7dm7UFX1qq3rnfpbQugv7uIPdo2kMcC', 'Administrator', 1, 1,
   '2018-01-19 15:26:53', '2018-01-19 15:26:54');