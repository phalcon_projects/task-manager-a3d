CREATE TABLE discussions
(
  id          INT(11) UNSIGNED PRIMARY KEY        NOT NULL AUTO_INCREMENT,
  project_id  INT(11) UNSIGNED                    NOT NULL,
  creator_id  INT(11) UNSIGNED                    NOT NULL,
  title       VARCHAR(255)                        NOT NULL,
  description TEXT                                NOT NULL,
  state       INT(11) DEFAULT '1'                 NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT project_discussions_projects_id_fk FOREIGN KEY (project_id) REFERENCES projects (id),
  CONSTRAINT project_discussions_users_id_fk FOREIGN KEY (creator_id) REFERENCES users (id)
);
CREATE INDEX creator_id
  ON discussions (creator_id);
CREATE UNIQUE INDEX project_discussions_id_uindex
  ON discussions (id);
CREATE INDEX project_id
  ON discussions (project_id);