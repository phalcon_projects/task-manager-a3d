DROP TABLE IF EXISTS project_user_roles;

CREATE TABLE project_user_roles
(
  id    INT(11) UNSIGNED PRIMARY KEY  NOT NULL AUTO_INCREMENT,
  title VARCHAR(255)                  NOT NULL,
  alias VARCHAR(255)                  NOT NULL,
  color VARCHAR(10) DEFAULT '#cccccc' NOT NULL
);

INSERT INTO project_user_roles (id, title, alias, color) VALUES (1, 'Administrator', 'administrator', '#ff0000');
INSERT INTO project_user_roles (id, title, alias, color) VALUES (2, 'User', 'user', '#a0a0a0');