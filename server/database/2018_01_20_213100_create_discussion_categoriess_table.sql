CREATE TABLE discussion_categories
(
  id            INT(11) UNSIGNED PRIMARY KEY        NOT NULL AUTO_INCREMENT,
  discussion_id INT(11) UNSIGNED                    NOT NULL,
  created_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at    TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT project_discussion_categories_project_discussions_id_fk FOREIGN KEY (discussion_id) REFERENCES discussions (id)
);
CREATE INDEX project_discussion_categories_discussion_id_index
  ON discussion_categories (discussion_id);