CREATE TABLE api_keys
(
    id int(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    value varchar(255) NOT NULL,
    state tinyint(4) DEFAULT '1' NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE UNIQUE INDEX api_keys_value_uindex ON api_keys (value);