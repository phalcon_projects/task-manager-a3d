CREATE TABLE task_modules
(
    id int(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    creator_id int(11) unsigned NOT NULL,
    task_id int(11) unsigned NOT NULL,
    type varchar(255) NOT NULL,
    source_data varchar(255) NOT NULL,
    source_type varchar(45) DEFAULT 'link' NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT task_modules_users_id_fk FOREIGN KEY (creator_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT task_modules_tasks_id_fk FOREIGN KEY (task_id) REFERENCES tasks (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX task_modules_users_id_fk ON task_manager_a3d.task_modules (creator_id);
CREATE INDEX task_modules_tasks_id_fk ON task_manager_a3d.task_modules (task_id);