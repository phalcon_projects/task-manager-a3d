CREATE TABLE project_settings
(
    id int(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    project_id int(11) unsigned NOT NULL,
    name varchar(255) NOT NULL,
    value text NULL,
    CONSTRAINT project_settings_projects_id_fk FOREIGN KEY (project_id) REFERENCES projects (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX project_settings_projects_id_fk ON project_settings (project_id);