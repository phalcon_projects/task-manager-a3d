CREATE TABLE notifications
(
  id              INT(11) UNSIGNED PRIMARY KEY        NOT NULL AUTO_INCREMENT,
  user_id         INT(11) UNSIGNED,
  object_id       INT(11) UNSIGNED,
  object_type     VARCHAR(50),
  object_link     VARCHAR(255),
  related_user_id INT(11) UNSIGNED,
  data            TEXT                                NOT NULL,
  state           TINYINT(4) DEFAULT '0'              NOT NULL,
  created_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT notifications_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE INDEX notifications_users_id_fk
  ON notifications (user_id);