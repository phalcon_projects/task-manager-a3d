CREATE TABLE timeline_sections
(
    id int(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    timeline_id int(11) unsigned NOT NULL,
    title varchar(255) NOT NULL,
    date_to timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    state tinyint(4) DEFAULT '1' NOT NULL,
    CONSTRAINT timeline_sections_timelines_id_fk FOREIGN KEY (timeline_id) REFERENCES timelines (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX timeline_sections_timelines_id_fk ON timeline_sections (timeline_id);