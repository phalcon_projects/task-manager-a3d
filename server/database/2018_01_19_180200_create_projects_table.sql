DROP TABLE IF EXISTS projects;

CREATE TABLE projects
(
  id          INT(11) UNSIGNED PRIMARY KEY        NOT NULL AUTO_INCREMENT,
  title       VARCHAR(255)                        NOT NULL,
  description TEXT,
  creator_id  INT(11) UNSIGNED                    NOT NULL,
  state       TINYINT(4) DEFAULT '1'              NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT projects_users_id_fk FOREIGN KEY (creator_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE INDEX projects_users_id_fk
  ON projects (creator_id);