CREATE TABLE timelines
(
    id int(11) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    object_id int(11) unsigned NOT NULL,
    object_type varchar(45) NOT NULL,
    start_date timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL
);