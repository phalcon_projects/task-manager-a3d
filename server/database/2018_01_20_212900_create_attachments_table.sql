CREATE TABLE attachments
(
  id          INT(11) UNSIGNED PRIMARY KEY        NOT NULL AUTO_INCREMENT,
  title       VARCHAR(255),
  description TEXT,
  path        VARCHAR(255)                        NOT NULL,
  type        VARCHAR(25)                         NOT NULL,
  creator_id  INT(11) UNSIGNED                    NOT NULL,
  project_id  INT(11) UNSIGNED,
  object_id   INT(11) UNSIGNED                    NOT NULL,
  object_type VARCHAR(40)                         NOT NULL,
  state       TINYINT(4) DEFAULT '1'              NOT NULL,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT attachments_users_id_fk FOREIGN KEY (creator_id) REFERENCES users (id),
  CONSTRAINT attachments_projects_id_fk FOREIGN KEY (project_id) REFERENCES projects (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE INDEX attachments_users_id_fk
  ON attachments (creator_id);
CREATE INDEX attachments_projects_id_fk
  ON attachments (project_id);