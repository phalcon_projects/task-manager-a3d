CREATE TABLE activities
(
  id              INT(11) UNSIGNED PRIMARY KEY        NOT NULL AUTO_INCREMENT,
  object_id       INT(11) UNSIGNED                    NOT NULL,
  object_type     VARCHAR(40)                         NOT NULL,
  creator_id       INT(11) UNSIGNED                    NOT NULL,
  related_id      INT(11) UNSIGNED,
  related_user_id INT(11) UNSIGNED,
  project_id      INT(11) UNSIGNED                    NOT NULL,
  type            VARCHAR(40)                         NOT NULL,
  data            TEXT                                NOT NULL,
  state           TINYINT(4) DEFAULT '1'              NOT NULL,
  created_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT activities_users_id_fk FOREIGN KEY (creator_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT activities_related_users_id_fk FOREIGN KEY (related_user_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT activities_projects_id_fk FOREIGN KEY (project_id) REFERENCES projects (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE INDEX activities_users_id_fk
  ON activities (creator_id);
CREATE INDEX activities_related_users_id_fk
  ON activities (related_user_id);
CREATE INDEX activities_projects_id_fk
  ON activities (project_id);