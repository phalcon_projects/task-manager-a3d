DROP TABLE IF EXISTS tasks;

CREATE TABLE tasks
(
  id          INT(11) UNSIGNED PRIMARY KEY        NOT NULL AUTO_INCREMENT,
  title       VARCHAR(255)                        NOT NULL,
  description TEXT,
  project_id  INT(11) UNSIGNED                    NOT NULL,
  creator_id  INT(11) UNSIGNED                    NOT NULL,
  assignee_id INT(11) UNSIGNED,
  primary_id  INT(11) UNSIGNED DEFAULT '0'        NOT NULL,
  state       TINYINT(4) DEFAULT '1'              NOT NULL,
  priority    TINYINT(4) DEFAULT '0'              NOT NULL,
  due_date    DATETIME,
  created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT tasks_projects_id_fk FOREIGN KEY (project_id) REFERENCES projects (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT tasks_users_id_fk FOREIGN KEY (creator_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE INDEX tasks_projects_id_fk
  ON tasks (project_id);
CREATE INDEX tasks_users_id_fk
  ON tasks (creator_id);