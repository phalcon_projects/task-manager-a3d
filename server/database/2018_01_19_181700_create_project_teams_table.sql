DROP TABLE IF EXISTS project_teams;

CREATE TABLE project_teams
(
  project_id INT(11) UNSIGNED,
  user_id    INT(11) UNSIGNED NOT NULL,
  user_role  INT(11) UNSIGNED NOT NULL,
  CONSTRAINT project_teams_projects_id_fk FOREIGN KEY (project_id) REFERENCES projects (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT project_teams_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT project_teams_project_user_roles_id_fk FOREIGN KEY (user_role) REFERENCES project_user_roles (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
CREATE INDEX project_teams_projects_id_fk
  ON project_teams (project_id);
CREATE INDEX project_teams_users_id_fk
  ON project_teams (user_id);
CREATE INDEX project_teams_project_user_roles_id_fk
  ON project_teams (user_role);