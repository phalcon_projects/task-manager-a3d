CREATE TABLE followers
(
  user_id     INT(11) UNSIGNED       NOT NULL,
  object_id   INT(11) UNSIGNED       NOT NULL,
  object_type VARCHAR(50)            NOT NULL,
  state       TINYINT(4) DEFAULT '1' NOT NULL,
  CONSTRAINT `PRIMARY` PRIMARY KEY (user_id, object_id),
  CONSTRAINT followers_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);