# Task manager A3D integration tool REST API service

## Initialize project

* Setup the server environment
* Setup the [Phalcon][1] extension for the PHP
* Initialize the database by running the migration from the `database` folder

[1]: https://phalconphp.com