const DB = require('./database')

class EntityService {
  constructor({ modelName } = {}) {
    this.modelName = modelName
    this.dbInstance = new DB()
  }

  create() {
    //this.dbConnection.query()
  }

  fetchList({ where = null, order = null, limit = null, fields = '*' } = {}) {
    this.dbInstance.query('SELECT * FROM ?', [this.modelName.toLowerCase()], (err, result, fields) => {
      if (err) {
        throw err
      }

      return result
    })
  }

  fetch({ where = null, order = null, limit = null } = {}) {
    if (!where.id) {
      throw new Error('Error entity fetch. Id is not specified.')
    } else {
      let query = `SELECT * FROM ${this.dbConnection.escape(this.modelName.toLowerCase())}`

      this.dbConnection.query(query, (err, result, fields) => {
        if (err) {
          throw err
        }

        return result
      })
    }
  }
}

module.exports = EntityService
