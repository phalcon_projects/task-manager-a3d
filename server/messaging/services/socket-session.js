class SocketSessionService
{
  constructor() {
    this.sessions = []
  }

  add(session) {
    if (session.socket_id && session.socket_id.length && !!session.user_id) {
      let isRefresh = false

      this.sessions.map(activeSession => {
        if (+activeSession.user_id === +session.user_id) {
          activeSession.socket_id = session.socket_id
          isRefresh = true
        }
      })

      if (!isRefresh) {
        this.sessions.push(session)
      }
    }
  }

  get() {
    return this.sessions
  }

  getBySocketId(socketId) {
    return this.sessions.find(session => session.socket_id === socketId)
  }

  getByUserId(userId) {
    return this.sessions.find(session => +session.user_id === +userId)
  }

  getUserIdBySocketId(socketId) {
    let session = this.getBySocketId(socketId)
    return session ? session.user_id : null
  }

  getSocketIdByUserId(userId) {
    let session = this.getByUserId(userId)
    return session ? session.socket_id : null
  }

  remove(socketId) {
    this.sessions.map((session, index) => {
      if (session.socket_id === socketId) {
        this.sessions.splice(index, 1)
      }
    })

  }
}

module.exports = SocketSessionService
