const dbConfig = require('../config/database.config')

module.exports = (Sequelize) => {
  return new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  })
}
