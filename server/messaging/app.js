const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session')

const app = express()

app.use(morgan('dev'))// log requests to the console
app.use(bodyParser.urlencoded({extended: true}))// get information
app.use(bodyParser.json())
app.use(cookieParser())// get cookies (needed for auth)
/*app.use(session({
  secret: 'SERVER_SESSION_SECRET',
  resave: false,
  saveUninitialized: false
}))*/

app.set('SERVER_SECRET_TOKEN', 'MESSAGING_SECRET_TOKEN')

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Access-Token, X-RequestService-With, Content-Type, Accept')
  if (req.method === 'Options') {
    res.header('Accept-Control-Allow-Methods', 'POST, PUT, DELETE')
    return res.status(200).json()
  }
  next()
})

module.exports = app
