const serverConfig = require('./config/server.config')
const app = require('./app')
const moment = require('moment')
const Sequelize = require('sequelize')
const sequelize = require('./services/database')(Sequelize)
const Op = require('sequelize').Op

const User = require('./models/user')(sequelize)
const Conversation = require('./models/conversation')(sequelize)
const ConversationParticipants = require('./models/conversation-participants')(sequelize)
const Message = require('./models/message')(sequelize)

sequelize.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })

app.get('/', (req, res) => {
  res.send('Server is working.')
})

const server = app.listen(serverConfig.port, serverConfig.host, () => {
  console.log(`The magic is happening at the server: http://${serverConfig.host}:${serverConfig.port}`)
})

//const http = require('http').Server(app)
const io = require('socket.io').listen(server)
const SocketSessionService = require('./services/socket-session')

socketSessions = new SocketSessionService()

io.use((socket, next) => {
  let token = socket.handshake.query.token
  let userId = socket.handshake.query.user_id

  if (token === 'CLIENT_AUTH_TOKEN') {
    console.log('SESSIONS', socketSessions.get())
    socketSessions.add({
      socket_id: socket.id,
      user_id: userId
    })
    return next()
  }

  return next(new Error('Authentication error'))
})

io.on('connection', socket => {
  console.log('CONNECTION SESSIONS', socketSessions.get())

  socket.on('disconnect', () => {
    let userId = socketSessions.getUserIdBySocketId(socket.id)
    socketSessions.remove(socket.id)
    console.log('AFTER REMOVE', socketSessions.get())
    fetchMemberList(userId)
  })

  socket.on('REQUEST_MESSAGING_CONVERSATION_INITIALIZE', data => {
    if (data.participants && data.participants.length) {

      let escapedCreatorId = sequelize.escape(data.participants[0])
      let escapedParticipantId = sequelize.escape(data.participants[1])
      sequelize.query(
        `SELECT conversation_id FROM conversation_participants WHERE user_id IN (${escapedCreatorId}, ${escapedParticipantId}) AND conversation_id NOT IN (SELECT conversation_id FROM conversation_participants WHERE user_id NOT IN (${escapedCreatorId}, ${escapedParticipantId}) GROUP BY conversation_id)`
      )
        .then(result => {
          let rawResult = result[0]
          let parsedResult = Object.assign({}, rawResult[0])// Group by in the raw query is not worked

          if (Object.keys(parsedResult).length) {
            if (parsedResult.conversation_id) {
              console.log('Conversation is fetched and initialized.')
              socket.emit('RESPONSE_MESSAGING_CONVERSATION_INITIALIZE', parsedResult.conversation_id)
            } else {
              throw Error('DB error. Cannot find conversation by id.')
            }
          } else {
            let dateNow = moment().format('YYYY-MM-DD HH:mm:ss')

            Conversation.create({
              title: 'Default',
              creator_id: data.participants[0],
              created_at: dateNow,
              updated_at: dateNow
            })
              .then(res => {
                data.participants.forEach(userId => {
                  ConversationParticipants.create({
                    conversation_id: res.get('id'),
                    user_id: userId
                  })
                })
                console.log('Conversation is created and initialized.')
                socket.emit('RESPONSE_MESSAGING_CONVERSATION_INITIALIZE', res.get('id'))
              })
              .catch(error => {
                throw error
              })
          }
        })
        .catch(error => {
          throw error
        })
    }
  })

  socket.on('REQUEST_MESSAGING_MEMBER_FETCH_LIST', data => {
    fetchMemberList()
  })

  function fetchMemberList(userId = null) {
    userId = userId || socketSessions.getUserIdBySocketId(socket.id)

    if (userId) {
      User.findAll({
        where: {
          id: { [Op.ne]: userId },
          state: 1
        }
      })
        .then(result => {
          result.map(item => {
            if (!socketSessions.getByUserId(item.id)) {
              item.state = 0
            }
          })
          socket.emit('RESPONSE_MESSAGING_MEMBER_FETCH_LIST', JSON.stringify(result))
        })
        .catch(error => {
          console.error(error)
        })
    } else {
      throw Error('Socket session was not found.')
    }
  }

  socket.on('REQUEST_MESSAGING_MESSAGE_CREATE', data => {
    Message.create(data)
      .then(message => {
        console.log('The message was created.')
        ConversationParticipants.find({
          where: {
            user_id: { [Op.ne]: data.creator_id },
            conversation_id: data.conversation_id
          }
        })
          .then(result => {
            if (result.user_id) {
              let participantSocket = socketSessions.getSocketIdByUserId(result.user_id)

              if (participantSocket) {
                socket.to(participantSocket).emit('RESPONSE_MESSAGING_MESSAGE_CREATE', message)
              }
            } else {
              throw new Error('Error fetching conversation participant.')
            }
          })
          .catch(error => {
            throw error
          })
        socket.emit('RESPONSE_MESSAGING_MESSAGE_CREATE', message)
      })
      .catch(error => {
        console.error(error)
      })
  })

  socket.on('REQUEST_MESSAGING_MESSAGE_REMOVE', message => {
    Message.update({ state: 0 }, { where: { id: message.id }, plain: true, returning: true })
      .then(result => {
        console.log('The message was removed.')
        ConversationParticipants.find({
          where: {
            user_id: { [Op.ne]: message.creator_id },
            conversation_id: message.conversation_id
          }
        })
          .then(result => {
            if (result.user_id) {
              let participantSocket = socketSessions.getSocketIdByUserId(result.user_id)

              if (participantSocket) {
                socket.to(participantSocket).emit('RESPONSE_MESSAGING_MESSAGE_REMOVE', message)
              }
            } else {
              throw new Error('Error fetching conversation participant.')
            }
          })
          .catch(error => {
            throw error
          })
        socket.emit('RESPONSE_MESSAGING_MESSAGE_REMOVE', message)
      })
      .catch(error => {
        console.error(error)
      })
  })

  socket.on('REQUEST_MESSAGING_MESSAGE_FETCH_LIST', data => {
    if (data.conversation_id) {

      Message.findAll({
        where: {
          conversation_id: data.conversation_id
        },
        include: [{
          model: User,
          as: 'creator'
        }],
        order: sequelize.col('created_at')
      })
        .then(result => {
          socket.emit('RESPONSE_MESSAGING_MESSAGE_FETCH_LIST', JSON.stringify(result))
        })
        .catch(error => {
          console.error(error)
        })
    }
  })
})
