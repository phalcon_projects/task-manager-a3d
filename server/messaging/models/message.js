const Sequelize = require('sequelize')

module.exports = (sequelize) => {
  const User = require('./user')(sequelize)

  let Message = sequelize.define('conversation_messages', {
    id: {
      type: Sequelize.DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    creator_id: {
      type: Sequelize.DataTypes.INTEGER
    },
    conversation_id: {
      type: Sequelize.DataTypes.INTEGER
    },
    content: {
      type: Sequelize.DataTypes.TEXT
    },
    state: {
      type: Sequelize.DataTypes.TINYINT
    },
    created_at: {
      type: Sequelize.DataTypes.DATE
    },
    updated_at: {
      type: Sequelize.DataTypes.DATE
    }
  }, { timestamps: false })

  Message.belongsTo(User, { foreignKey: 'creator_id', as: 'creator' })

  return Message
}
