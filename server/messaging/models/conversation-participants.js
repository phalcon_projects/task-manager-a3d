const Sequelize = require('sequelize')

module.exports = (sequelize) => {
  return ConversationParticipants = sequelize.define('conversation_participants', {
    conversation_id: {
      type: Sequelize.DataTypes.INTEGER,
      primaryKey: true
    },
    user_id: {
      type: Sequelize.DataTypes.INTEGER,
      primaryKey: true
    }
  }, { timestamps: false })
}
