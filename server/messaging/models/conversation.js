const Sequelize = require('sequelize')

module.exports = (sequelize) => {
  return sequelize.define('conversation', {
    id: {
      type: Sequelize.DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    creator_id: {
      type: Sequelize.DataTypes.INTEGER
    },
    title: {
      type: Sequelize.DataTypes.STRING
    },
    created_at: {
      type: Sequelize.DataTypes.DATE
    },
    updated_at: {
      type: Sequelize.DataTypes.DATE
    }
  }, { timestamps: false })
}
