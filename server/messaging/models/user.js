const Sequelize = require('sequelize')

module.exports = (sequelize) => {
  return sequelize.define('user', {
    id: {
      type: Sequelize.DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    login: {
      type: Sequelize.DataTypes.STRING
    },
    email: {
      type: Sequelize.DataTypes.STRING
    },
    name: {
      type: Sequelize.DataTypes.STRING
    },
    role: {
      type: Sequelize.DataTypes.TINYINT
    },
    state: {
      type: Sequelize.DataTypes.TINYINT
    },
    created_at: {
      type: Sequelize.DataTypes.DATE
    },
    updated_at: {
      type: Sequelize.DataTypes.DATE
    }
  }, { timestamps: false })
}
