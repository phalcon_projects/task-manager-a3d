module.exports = {
  host: 'HOST',
  database: 'DB_NAME',
  username: 'DB_USER_NAME',
  password: 'DB_PASSWORD'
}
